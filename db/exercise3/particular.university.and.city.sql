SELECT student.roll_no
	  ,student.stu_name
      ,student.gender
      ,student.stu_dob
      ,student.stu_email
      ,student.stu_phone
      ,student.stu_address 
      ,college.college_name AS "College Name"
      ,department.dept_name AS "Department Name"
      ,employee.emp_name AS "HOD Name"
  FROM student student
  LEFT JOIN college_department college_department
    ON student.cdept_id = college_department.cdept_id
  LEFT JOIN college college
    ON student.college_id = college.college_id
 INNER JOIN department department
    ON college_department.udept_code = department.dept_code
 INNER JOIN employee employee
    ON college.college_id = employee.college_id
 WHERE desig_id = '3'
 GROUP BY roll_no
 LIMIT 20