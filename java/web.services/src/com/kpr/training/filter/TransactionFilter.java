package com.kpr.training.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.kpr.training.exception.AppException;
import com.kpr.training.exception.ErrorCodes;
import com.kpr.training.service.ConnectionService;

public class TransactionFilter implements Filter {
	
	private FilterConfig filter = null;

	@Override
	public void destroy() {
		filter = null;
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		try {
			ConnectionService.get();
			chain.doFilter(req, res);
		} catch (Exception e) {
			throw new AppException(ErrorCodes.E438);
		}
	}

	@Override
	public void init(FilterConfig filter) throws ServletException {
		this.filter = filter;
	}

}
