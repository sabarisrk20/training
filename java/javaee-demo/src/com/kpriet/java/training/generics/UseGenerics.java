package com.kpriet.java.training.generics;

/**
 * Problem Statement
 * 1.What will be the output of the following program?
 * 
 * Requirement
 * 1.What will be the output of the following program?
 * 
 * Methods Signature
 * 1.void set(T var);
 * 2.T get();
 * 
 * Entity
 * 1.UseGenerics
 * 2.MyGen
 * 
 * Answer
 * 1.No This Program will not execute and give output since the Generic object created for MyGen class is of Integer type 
 *   and the set method of MyGen Class takes the Integer type data, but the input gave is of String type, so the Compiler shows 
 *   error in the Compile Time..
 * 
 *
 */


public class UseGenerics {
	
	public static void main(String args[]){  
		/*
        MyGen<Integer> m = new MyGen<Integer>();  
        m.set("merit");
        System.out.println(m.get());*/
    }
}

class MyGen<T>
{
    T var;
    void  set(T var)
    {
        this.var = var;
    }
    T get()
    {
        return var;
    }
}
