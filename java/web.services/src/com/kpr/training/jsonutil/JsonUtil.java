package com.kpr.training.jsonutil;

import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kpr.training.jdbc.model.Person;
import com.kpr.training.validator.Validator;

public class JsonUtil {
	String myDateFormat = "yyyy-MM-dd";
			
	public Object stringToJson(String personJson, Object typeObj) {
		
		
		Gson gson = new GsonBuilder().setDateFormat(myDateFormat).create();
		
		return gson.fromJson( personJson, typeObj.getClass());
	}
	
	public String jsonToString(Object typeObj) {
		
		Gson gson = new GsonBuilder().setDateFormat(myDateFormat).create();
		
		return gson.toJson(typeObj);
	}

}
