package com.kpriet.java.training.map;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.SortedMap;
import java.util.TreeMap;




/**
 * Problem Statement
 * 1.Write a Java program to get the portion of a map whose keys range 
 *   from a given key to another key
 *   
 * Requirement
 * 1.Write a Java program to get the portion of a map whose keys range 
 *   from a given key to another key
 *   
 * Method Signature
 *   
 * Entity
 * 1.SubMap 
 * 
 * Jobs to be Done
 * 1.Create a new TreeMap called oldMap in which the values can be accessed dynamically.
 * 2.Add values to the oldMap using put method.
 * 3.Get two user value access using subMap. 
 * 4.Access the oldMap value using subMap function provided by the Map Interface.
 * 5.At the last line the oldMap has passed with subMap and passing the starting and ending value
 * 6.Pass the startVal and endVal got from the user to the subMap function and printing the values in the console. 
 * 
 * Pseudo Code
 * class SubMap {
 *	
 *	public static void main(String[] args) {
 *		//Scanner Class
 *		Scanner scan = new Scanner(System.in);
 *		
 *		//Sorted TreeMap 
 *		SortedMap<Integer,String> oldMap = new TreeMap<Integer,String>();
 *		
 *		//Adding Values to the TreeMap
 *		oldMap.put(1,"Tom and Jerry");
 *		oldMap.put(2,"Shin Chan");
 *		oldMap.put(3,"Ben 10");
 *		oldMap.put(4,"Jackie Chan");
 *		oldMap.put(5,"Ninja Hattori");
 *		oldMap.put(6,"Teen Titans");
 *		oldMap.put(7,"Kick Buttowski");
 *		oldMap.put(8,"Kit vs kat");
 *		oldMap.put(9,"Power Rangers SPD"); 
 *		oldMap.put(10,"Power Rangers Majestic Force");
 *		
 *		//Getting User Values to Access items
 *		System.out.println("Enter the Range of the Map to access!!");
 *		int startVal = scan.nextInt();
 *		int endVal = scan.nextInt();
 *		//Accessing the values of the map using subMap.
 *		System.out.println(oldMap.subMap(startVal, endVal).values());
 *		
 *	}
 *
 *}
 *
 *
 *
 *
 */

public class SubMap {
	
	public static void main(String[] args) {
		//Scanner Class
		Scanner scan = new Scanner(System.in);
		
		//Sorted TreeMap 
		SortedMap<Integer,String> oldMap = new TreeMap<Integer,String>();
		
		//Adding Values to the TreeMap
		oldMap.put(1,"Tom and Jerry");
		oldMap.put(2,"Shin Chan");
		oldMap.put(3,"Ben 10");
		oldMap.put(4,"Jackie Chan");
		oldMap.put(5,"Ninja Hattori");
		oldMap.put(6,"Teen Titans");
		oldMap.put(7,"Kick Buttowski");
		oldMap.put(8,"Kit vs kat");
		oldMap.put(9,"Power Rangers SPD");
		oldMap.put(10,"Power Rangers Majestic Force");
		
		//Getting User Values to Access items
		System.out.println("Enter the Range of the Map to access!!");
		int startVal = scan.nextInt();
		int endVal = scan.nextInt();
		//Accessing the values of the map using subMap.
		System.out.println(oldMap.subMap(startVal, endVal).values());
		
	}

}
