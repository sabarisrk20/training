CREATE TABLE `department_table` (
   `id` INT NOT NULL
  ,`name` VARCHAR(45) NOT NULL
  ,PRIMARY KEY (`id`));


 CREATE TABLE `staff_table` (
   `staff_id` INT NOT NULL
  ,`name` VARCHAR(45) NOT NULL
  ,`subject` VARCHAR(45) NOT NULL
  ,`dob`  VARCHAR(45) NOT NULL
  ,PRIMARY KEY (`staff_id`));


CREATE TABLE `student_table` (
   `roll_no` INT NOT NULL
  ,`name` VARCHAR(45) NOT NULL
  ,`dob` date NOT NULL
  ,`year_of_joining` VARCHAR(45) NOT NULL
  ,`staff_id` INT 
  ,`dept_id` INT 
  ,PRIMARY KEY (`roll_no`)
  ,FOREIGN KEY (`staff_id`) REFERENCES staff(`id`)
  ,FOREIGN KEY (`dept_id`) REFERENCES department1(`id`));
  
  
ALTER TABLE `student_table` 
RENAME TO  `student` ;
  
ALTER TABLE `staff_table` 
RENAME TO  `staff` ;

ALTER TABLE `department_table` 
RENAME TO  `department` ;  


DROP TABLE `department`;
DROP TABLE `staff`;
DROP TABLE `student`;

TRUNCATE TABLE `department`;
TRUNCATE TABLE `staff`;
TRUNCATE TABLE `student`;