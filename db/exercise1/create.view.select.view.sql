CREATE VIEW All_Details AS
SELECT department.name AS 'Department Name' , student.name AS 'Student Name' , staff.name AS 'Staff Name'
FROM department
JOIN student ON student.department_id = department.id
JOIN staff ON staff.id = student.staff_id;

CREATE VIEW staff_dateails AS
SELECT id,name 
FROM staff;

CREATE VIEW stud_details AS
SELECT roll_no,name,staff_id
FROM student;

SELECT student_details.roll_no AS 'Roll No'
      ,student_details.student_name AS 'Student Name'
      ,staff_details.staff_name AS 'Staff Name' 
  FROM staff_details staff_details 
      ,stud_details student_details
 WHERE staff_details.id = student_details.staff_id
 ORDER BY student_details.roll_no;
 
DROP VIEW staff_details;
DROP VIEW stud_details;
DROP VIEW all_details;