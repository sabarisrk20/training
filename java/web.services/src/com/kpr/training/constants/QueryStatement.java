package com.kpr.training.constants;

public class QueryStatement {
	
	public static final String INSERT_ADDRESS_QUERY     = new StringBuilder("INSERT INTO address ")
																    .append("(street             ")
																    .append(", city              ")
																    .append(", postal_code)      ")
																    .append("VALUES (?,?,?)      ")
																    .toString();
																			
	public static final String UPDATE_ADDRESS_QUERY     = new StringBuilder("UPDATE address 	")
																    .append("   SET street = (?)")
																    .append(",        city = (?)")
																    .append(", postal_code = (?)")
																    .append(" WHERE (id = (?))  ")
																    .toString();
																
	public static final String READ_ADDRESS_QUERY      = new StringBuilder("SELECT street       ")
															       .append(", 	   city         ")
															       .append(",      postal_code  ")
															       .append("  FROM address      ")
															       .append(" WHERE id = (?)     ")
															       .toString();
																			
	public static final String READ_ALL_ADDRESS_QUERY  = new StringBuilder("SELECT id     	   ")
																   .append("      ,street 	   ")
			 													   .append("      ,city        ")
			 													   .append("      ,postal_code ")
			 													   .append("  FROM address	   ")
			 													   .toString();
																	 
	public static final String DELETE_ADDRESS_QUERY    = new StringBuilder("DELETE FROM address")
																   .append(" WHERE (id = (?))  ")
																   .toString();
					 
	
	public static final String INSERT_PERSON_QUERY     = new StringBuilder("INSERT INTO person	   ")
															       .append("           (first_name ")
															       .append("   	       ,last_name  ")
															   	   .append("		   ,email      ")
															   	   .append("		   ,address_id ")
															   	   .append("		   ,birth_date")
															   	   .append("           ,user_id)    ")
															       .append("VALUES (?,?,?,?,?,?)   ")
															       .toString();
																				  
	public static final String READ_PERSON_QUERY       = new StringBuilder("SELECT 	  id          ")
															 	 .append("			, first_name  ")
															 	 .append("			, last_name   ")
															 	 .append("			, email       ")
															 	 .append("          , address_id  ")
															 	 .append("          , birth_date  ")
															 	 .append("          , created_date")
															 	   .append("  FROM person         ")
															 	   .append(" WHERE  id =  ?       ")
															 . toString();
															
																			
	public static final String READ_ALL_PERSON_QUERY   = new StringBuilder("SELECT  id	          ")
																   .append("      , first_name    ")
																   .append("      , last_name     ")
															       .append("	  , email         ")
																   .append("	  , address_id    ")
																   .append("	  , birth_date    ")
																   .append("	  , created_date  ")
																   .append("  FROM person         ")
																   .toString();
																			 
	
	public static final String DELETE_PERSON           = new StringBuilder("DELETE FROM person")
																   .append(" WHERE  id =  ?   ")
																   .toString();
																		
	
	public static final String UPDATE_PERSON 		   = new StringBuilder("UPDATE person 		  ")
																   .append("   SET first_name = ? ")
																   .append(",      last_name = ?  ")
																   .append(",      email = ?	  ")
																   .append(",      birth_date = ? ")
																   .append(" WHERE id = ?		  ")
																   .toString();
																		
	public static final String EMAIL_CHECKER 		   = new StringBuilder("SELECT id			 ")
																   .append("  FROM person		 ")
																   .append(" WHERE  email =  ?   ")
																   .append("   AND id !=  ?      ")
																   .toString();
	

	public static final String SEARCH_QUERY 		   = new StringBuilder("SELECT  id 			 ")
																   .append(",		street       ")
																   .append(",		city       	 ")
																   .append(",	    postal_code  ")
																   .append("  FROM address       ")
																   .append(" WHERE  street       ")
																   .append("  LIKE ?	  	     ")
															       .append("    OR  city         ")
																   .append("  LIKE ? 	   	     ")
																   .append("    OR  postal_code  ")
																   .append("  LIKE ? 	         ")
																   .toString();
	
	
	public static final String ADDRESS_CHECK 		   = new StringBuilder("SELECT id		      ")
														 		   .append("  FROM address	   	  ")
														           .append(" WHERE street = ?	  ")
														           .append("   AND city = ?  	  ")
														           .append("   AND postal_code = ?")
														           .toString();
	
	public static final String PERSON_CHECKER		   = new StringBuilder("SELECT id			 ")
																   .append("  FROM person		 ")
																   .append(" WHERE first_name = ?")
																   .append("   AND last_name = ? ")
																   .toString();

	public static final String ADDRESS_ID_CHECKER      = new StringBuilder("SELECT id      	  	 ")
																   .append("  FROM person        ")
																   .append(" WHERE address_id = ?")
																   .toString();
	
	//User Queries
	
	public static final String INSERT_USER_QUERY     = new StringBuilder("INSERT INTO user       ")
																 .append("           (user_role  ")
																 .append("  	      ,user_name ")
																 .append("  	      ,password) ")
																 .append("VALUES (?,?,?)         ")
																 .toString();
	
	public static final String READ_USER_QUERY      = new StringBuilder("SELECT id         ")
															    .append(", 	    user_name  ")
															    .append(",      user_role  ")
															    .append("  FROM user       ")
															    .append(" WHERE id = (?)   ")
															    .toString();
	
	public static final String READ_ALLUSER_QUERY      = new StringBuilder("SELECT id         ")
																   .append(", 	   user_name  ")
																   .append(",      user_role  ")
																   .append("  FROM user       ")
																   .toString();
	
	public static final String GET_USER_ID_QUERY        = new StringBuilder("SELECT id           ")
														           .append("  FROM user          ")
														           .append(" WHERE user_name = ? ")
														           .append("   AND password = ?  ")
														           .toString();
}