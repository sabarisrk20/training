/*To Test All possible conditions for Person Service 
Requirements:
  Perform Test Case
Entity:
  * Person
  * Address
  * PersonService
  * AppException
  * ErrorCode

Method Signature:
   * public static void setUp() throws Exception
   * public void insertPersonDiffAddress() throws Exception
   * public void updatePersonWithAddress() throws Exception
   * public void readPersoWithAddressn() throws Exception
   * public void readPersoWithoutAddressn() throws Exception
   * public static void readAllPerson() throws Exception
   * public void AddPersonWithuniqueEmail() throws Exception
   * public void AddPersonWithuniqueName() throws Exception
   * public void valuesEmpty() throws Exception
   * public void invalidDateFormate() throws Exception

Jobs to be Done:
   * Create a Class name PersonServiceTest
   * Create a Thread name thread1,thread2,thread3.
   * In public void insertPersonDiffAddress() throws Exception method
	    -> Create a object for Address and Person name person,address.
	    -> Create  the object for PersonServie name ps
	    -> Set the values for name,email,,city,street,postal_code,birth_date.
	    -> Check the date formate
	    -> Call the insertPerson() method and return type is stored in insertPersonStatus.
		-> Set assert is True if insertPersonStatus > 0.
		-> check if createPersonStatus > 0
			=> call the method commitRollback() and pass the parameter is true
		-> else 
			=> call the method commitRollback() and pass the parameter is false
		
  *	In method public void insertPersonWithSameAddress() throws Exception
	    -> Create a object for Address and Person name person,address.
	    -> Create  the object for PersonServie name ps
	    -> Set the values for name,email,,city,street,postal_code,birth_date.
	    -> Check the date formate
	    -> Call the insertPerson() method and return type is stored in insertPersonStatus.
		-> Set assert is True if insertPersonStatus > 0.
		-> check if createPersonStatus > 0
			=> call the method commitRollback() and pass the parameter is true
		-> else 
			=> call the method commitRollback() and pass the parameter is false
						
  * In method public void updatePersonWithAddress() throws Exception
        -> Create  the object for PersonServie name ps
	    -> Create a object for Address and Person name person,address.
	    -> Set the values for a,b,name,email,,city,street,postal_code,birth_date.
	    -> Check the date formate
	    -> Call the insertPerson() method and return type is stored in insertPersonStatus.
		-> check if createPersonStatus > 0
			=> call the method commitRollback() and pass the parameter is true
		-> else 
			=> call the method commitRollback() and pass the parameter is false	     
  * In Method public void readPersoWithAddressn() throws AppException, SQLException
		-> set the value for  a
		-> Create  the object for PersonServie name personService
	    -> Call the readPerson() pass true as parameter in method and return type is stored in objectList.
		-> Set assert is True if objectList ! = null.	
  * In method public void readPersonWithoutAddress() throws Exception
		-> set the value for  a
		-> Create  the object for PersonServie name personService
	    -> Call the readPerson() pass true as parameter in method and return type is stored in objectList.
		-> Set assert is True if objectList ! = null.	
  * In method public void readAPerson() throws Exception
		-> set the value for  a
		-> Create  the object for PersonServie name personService
	    -> Call the readAllPerson()
PsudoCode:


public class PersonServiceTest {
	static PersonServices ps;
	long createPersonStatus = 0;
	boolean updateStatus = false;

	@BeforeTest
	public static void setUp() throws Exception {
		ps = new PersonServices();

	}

	@Test
	public void insertPersonCSVFile() throws Exception {
		IncludePerson person = new IncludePerson();
		person.includePerson();
		
	}

	@Test(priority = 1, description = "Person Insertion with Different Address")
	public void insertPersonDiffAddress() throws Exception {
		Address address = new Address("trichyroad", "salem", 636006);
		Person person = new Person("Sunil", "Kumar", "Sunil@hotmail.com", PersonValidator.dateFormate("21-05-2001"),
				address);
		createPersonStatus = ps.create(person);
		Assert.assertTrue(createPersonStatus > 0);
		if(createPersonStatus > 0) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
		
	}

	@Test(priority = 2, description = "Person Insertion with Same Address")
	public void insertPersonWithSameAddress() throws Exception {
		Address address = new Address("trichyroad", "salem", 636006);
		Person person = new Person("Kavin", "Kumar", "Kavin@hotmail.com", PersonValidator.dateFormate("24-12-2001"),
				address);
		createPersonStatus = ps.create(person);
		Assert.assertTrue(createPersonStatus > 0);
		if(createPersonStatus > 0) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
	}

	@Test(priority = 3, description = "Person Update with Address")
	public void updatePersonWithAddress() throws Exception {
		Address address = new Address("ravi road", "chennai", 60006);
		address.setId(5);
		Person person = new Person("dinesh", "kumar", "Kavin@gmailmail.com", PersonValidator.dateFormate("24-12-2001"),
				address);
		person.setId(5);

		List<Object> personList = ps.read(5, true);
		List<Object> updatedPersonList = ps.read(5, true);
		ps.update(person);
		Assert.assertNotEquals(personList, updatedPersonList);
		if(personList != updatedPersonList) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
	}

	@Test(priority = 4, description = "Read Particular Person with address")
	public void readPersoWithAddressn() throws Exception {
		long a = 2;
		List<Object> personList = ps.read(a, true);
		System.out.println(personList.toString());
		Assert.assertTrue(personList.toString() != null);
	}

	@Test(priority = 5, description = "Read Particular Person with out address")
	public void readPersonWithoutAddress() throws Exception {
		long a = 2;
		List<Object> personList = ps.read(a, false);
		System.out.println(personList.toString());
		Assert.assertTrue(personList.toString() != null);
	}

	@Test(priority = 6, description = "Read All Person with address")
	public void readAllPerson() throws Exception {
		ArrayList<Object> personList = ps.readAll();
		Stream.of(personList.toString()).forEach(System.out::println);
		Assert.assertTrue(personList.toString() != null);

	}

	@Test(priority = 7, description = "ADD Person with same email with Address", expectedExceptions = {
			AppException.class })
	public void AddPersonWithuniqueEmail() throws Exception {
		Address address = new Address("trichyroad", "salem", 636006);
		Person person = new Person("Kavin", "Raj", "Kavin@hotmail.com", PersonValidator.dateFormate("24-12-2001"),
				address);
		createPersonStatus = ps.create(person);
		Assert.assertTrue(createPersonStatus == 0);
		if(createPersonStatus > 0) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
	}

	@Test(priority = 8, description = "ADD Person with same first and last namewith Address", expectedExceptions = {
			AppException.class })
	public void AddPersonWithuniqueName() throws Exception {
		Address address = new Address("trichyroad", "salem", 636006);
		Person person = new Person("Kumar", "Kumar", "Kavin@hotmail.com", PersonValidator.dateFormate("24-12-2001"),
				address);
		createPersonStatus = ps.create(person);
		Assert.assertTrue(createPersonStatus == 0);
		if(createPersonStatus > 0) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
	}

	@Test(priority = 9, description = "ADD Person with same firstname,lastname,dob are empty with Address", expectedExceptions = {
			AppException.class })
	public void valuesEmpty() throws Exception {
		Address address = new Address("trichyroad", "salem", 636006);
		Person person = new Person(null, null, "Kavin@hotmail.com", PersonValidator.dateFormate("24-12-2001"), address);
		createPersonStatus = ps.create(person);
		Assert.assertTrue(createPersonStatus == 0);
		if(createPersonStatus > 0) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
	}

	@Test(priority = 10, description = "ADD Person with invalid dob formate with Address", expectedExceptions = {
			AppException.class })
	public void invalidDateFormate() throws Exception {
		Address address = new Address("trichyroad", "salem", 636006);
		Person person = new Person("vel", "Kumar", "Kavin@hotmail.com", PersonValidator.dateFormate("2001-05-21"),
				address);
		createPersonStatus = ps.create(person);
		Assert.assertTrue(createPersonStatus == 0);
		if(createPersonStatus > 0) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
	}

}

*/
package com.kpr.training.testing;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.Assert;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import com.kpr.training.exception.AppException;
import com.kpr.training.model.Address;
import com.kpr.training.model.Person;
import com.kpr.training.services.ConnectionService;
import com.kpr.training.services.PersonServices;
import com.kpr.training.validator.PersonValidator;

public class PersonServiceTest {
	static PersonServices ps;
	long createPersonStatus = 0;
	boolean updateStatus = false;

	@BeforeTest
	public static void setUp() throws Exception {
		ps = new PersonServices();

	}

	@Test
	public void insertPersonCSVFile() throws Exception {
		IncludePerson person = new IncludePerson();
		person.includePerson();
		
	}

	@Test(priority = 1, description = "Person Insertion with Different Address")
	public void insertPersonDiffAddress() throws Exception {
		Address address = new Address("trichyroad", "salem", 636006);
		Person person = new Person("Sunil", "Kumar", "Sunil@hotmail.com", PersonValidator.dateFormate("21-05-2001"),
				address);
		createPersonStatus = ps.create(person);
		Assert.assertTrue(createPersonStatus > 0);
		if(createPersonStatus > 0) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
		
	}

	@Test(priority = 2, description = "Person Insertion with Same Address")
	public void insertPersonWithSameAddress() throws Exception {
		Address address = new Address("trichyroad", "salem", 636006);
		Person person = new Person("Kavin", "Kumar", "Kavin@hotmail.com", PersonValidator.dateFormate("24-12-2001"),
				address);
		createPersonStatus = ps.create(person);
		Assert.assertTrue(createPersonStatus > 0);
		if(createPersonStatus > 0) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
	}

	@Test(priority = 3, description = "Person Update with Address")
	public void updatePersonWithAddress() throws Exception {
		Address address = new Address("ravi road", "chennai", 60006);
		address.setId(5);
		Person person = new Person("dinesh", "kumar", "Kavin@gmailmail.com", PersonValidator.dateFormate("24-12-2001"),
				address);
		person.setId(5);

		List<Object> personList = ps.read(5, true);
		List<Object> updatedPersonList = ps.read(5, true);
		ps.update(person);
		Assert.assertNotEquals(personList, updatedPersonList);
		if(personList != updatedPersonList) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
	}

	@Test(priority = 4, description = "Read Particular Person with address")
	public void readPersoWithAddressn() throws Exception {
		long a = 2;
		List<Object> personList = ps.read(a, true);
		System.out.println(personList.toString());
		Assert.assertTrue(personList.toString() != null);
	}

	@Test(priority = 5, description = "Read Particular Person with out address")
	public void readPersonWithoutAddress() throws Exception {
		long a = 2;
		List<Object> personList = ps.read(a, false);
		System.out.println(personList.toString());
		Assert.assertTrue(personList.toString() != null);
	}

	@Test(priority = 6, description = "Read All Person with address")
	public void readAllPerson() throws Exception {
		ArrayList<Object> personList = ps.readAll();
		Stream.of(personList.toString()).forEach(System.out::println);
		Assert.assertTrue(personList.toString() != null);

	}

	@Test(priority = 7, description = "ADD Person with same email with Address", expectedExceptions = {
			AppException.class })
	public void AddPersonWithuniqueEmail() throws Exception {
		Address address = new Address("trichyroad", "salem", 636006);
		Person person = new Person("Kavin", "Raj", "Kavin@hotmail.com", PersonValidator.dateFormate("24-12-2001"),
				address);
		createPersonStatus = ps.create(person);
		Assert.assertTrue(createPersonStatus == 0);
		if(createPersonStatus > 0) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
	}

	@Test(priority = 8, description = "ADD Person with same first and last namewith Address", expectedExceptions = {
			AppException.class })
	public void AddPersonWithuniqueName() throws Exception {
		Address address = new Address("trichyroad", "salem", 636006);
		Person person = new Person("Kumar", "Kumar", "Kavin@hotmail.com", PersonValidator.dateFormate("24-12-2001"),
				address);
		createPersonStatus = ps.create(person);
		Assert.assertTrue(createPersonStatus == 0);
		if(createPersonStatus > 0) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
	}

	@Test(priority = 9, description = "ADD Person with same firstname,lastname,dob are empty with Address", expectedExceptions = {
			AppException.class })
	public void valuesEmpty() throws Exception {
		Address address = new Address("trichyroad", "salem", 636006);
		Person person = new Person(null, null, "Kavin@hotmail.com", PersonValidator.dateFormate("24-12-2001"), address);
		createPersonStatus = ps.create(person);
		Assert.assertTrue(createPersonStatus == 0);
		if(createPersonStatus > 0) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
	}

	@Test(priority = 10, description = "ADD Person with invalid dob formate with Address", expectedExceptions = {
			AppException.class })
	public void invalidDateFormate() throws Exception {
		Address address = new Address("trichyroad", "salem", 636006);
		Person person = new Person("vel", "Kumar", "Kavin@hotmail.com", PersonValidator.dateFormate("2001-05-21"),
				address);
		createPersonStatus = ps.create(person);
		Assert.assertTrue(createPersonStatus == 0);
		if(createPersonStatus > 0) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
	}

}
