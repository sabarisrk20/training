package com.kpriet.java.training.lambda;


/**
 * Problem Statement
 * 1.which one of these is a valid lambda expression? and why?
 *   (int x, int y) -> x+y; or (x, y) -> x + y;
 * 2.What is wrong with the following interface? and fix it, (int x, y) -> x + y;
 * 3.Convert this following code into a simple lambda expression
 *      int getValue(){
 *          return 5;
 *      }
 *      
 * Entity
 * 1.NumbersHolder Interface
 * 2.LambdaSnippet Class
 * 
 * Work Done
 * 1.Created a Interface called NumbersHolder and it has a getValue abstract method.
 * 2.Changed the Given Snippet to the Functional Interface and called the getValue method with 
 *   the Lambda Expression with x as a parameter and returned to x to the Console.
 *   
 * Answer
 * 1.Both the Expressions are Correct and Valid Lambda Expression
 *   (int x, int y) -> x+y;
 *   (x,y) -> x+y;
 * 2.The Expression is not Valid since either the data type should be specifed or not
 *   (int x, y) -> x + y;
 *
 */

interface NumbersHolder {
	int getValue(int x);
}

public class LambdaSnippet {
	/*
	int getValue(){
        return 5;
    }
	*/
	public static void main(String[] args) {
		NumbersHolder hold = (x) -> {
			return x;
		};
		
		hold.getValue(5);
	}

}
