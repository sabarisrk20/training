package com.kpriet.java.training.array.exception;

/**
 * Problem Statement
 * 1.Handle and give the reason for the exception in the following code
 * 
 * Entity
 * 1.Exception Class
 * 
 * Work Done
 * 1.Created a Class called Exception as guided.
 * 2.Created a integer array called arr and passed 5 values.
 * 3.Printing the 7th index value of the array
 * 
 * Answer:
 * 1.The Following Code throws ArrayIndexOutOfBoundsException since the arr Array Contains only 5 values and accesing
 *   the 7th index element is not possible.
 * 2.This can be solved by catching the ArrayIndexOutOfBounds Exception in the code.
 * 
 * Pseudo Code
 * class Exception {
 *	
 *		public static void main(String[] args) {
 *			try {
 *				int arr[] = {1,2,3,4,5};
 *				System.out.println(arr[7]);
 *			} catch (ArrayIndexOutOfBoundsException e) {
 *				System.out.println("Access the Value inside the Array Size");
 *			}
 *		}
 *
 *	}
 * @author SRK
 *
 */
public class Exception {
	
	public static void main(String[] args) {
		try {
			int arr[] = {1,2,3,4,5};
			System.out.println(arr[7]);
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Access the Value inside the Array Size");
		}
	}

}
