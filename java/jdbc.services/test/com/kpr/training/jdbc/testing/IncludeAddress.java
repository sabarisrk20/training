package com.kpr.training.jdbc.testing;
import java.io.Reader;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import java.nio.file.Files;
import java.nio.file.Paths;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.services.AddressServices;

public class IncludeAddress{
	public void includeAddress() throws Exception {
		AddressServices addressService = new AddressServices();
		try (
	            Reader reader = Files.newBufferedReader(Paths.get("C:/Users/Sunil kumar/eclipse-workspace/jdbc.services/resorces/addressinputs.csv"));
		        CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT
		                    .withFirstRecordAsHeader()
		                    .withIgnoreHeaderCase()
		                    .withTrim());
	        ) {
			
	            for (CSVRecord csvRecord : csvParser) {
	                String street = csvRecord.get("street");
	                String city = csvRecord.get("city");
	                String postal_code = csvRecord.get("postal_code");
	                int code = Integer.parseInt(postal_code);
	                Address address = new Address(street,city,code);
	                addressService.create(address);
	            }
	        }
	}
	
}

