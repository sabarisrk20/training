import java.util.Scanner;

import com.java.training.core.converter.Time;

class Distance {
    private float Metre;
    private float KM;
    private float Mile;

    public void printDistanceConverter() {
        System.out.println("The Value of Metre is"+this.Metre);
        System.out.println("The Value of KM is"+this.KM);
        System.out.println("The Value of Mile is"+this.Mile); 
    }
    
    public void KMcoverter() {
        Scanner sc=new Scanner(System.in);
        System.out.println("1:KM to Metre,2:Mile to KM");
        int ch=sc.nextInt();
        switch(ch) {
            case 1:
                System.out.println("Enter the KM value");
                KM=sc.nextFloat();
                this.Metre = KM * 1000f;
                break;
            case 2:
                System.out.println("Enter the Mile value");
                Mile=sc.nextFloat();
                KM = Mile * 1.6f;
                break;
            default:
                System.out.println("enter a valid number");        
        }
    }
    
}

public class CurrencyConverter extends Distance{
    public void callClass() {
        Scanner scan = new Scanner(System.in);
        Convertor con = new Convertor(0.0f,0.0f,0.0f,0.0f,0);
        int currencyChoice;
        int conChoice = 1;
        do {
            System.out.println("CurrencyConverter ");
            System.out.println("1.Inr Conversion 2.Dollar Conversion 3.Euro Conversion 4.Yen Conversion\n Enter Your Choice:");
            currencyChoice = scan.nextInt();
            switch(currencyChoice){
                case 1:
                    con.inrConvertor();
                    con.printInrCurrency();
                    break;
                case 2:
                    con.dollarConvertor();
                    con.printDollarCurrency();
                    break;
                case 3:
                    con.euroConvertor();
                    con.printEuroCurrency();
                    break;
                case 4:
                    con.yenConvertor();
                    con.printYenCurrency();
                    break;
                default:
                    System.out.println("Enter the given option");
                    continue;
            }
            System.out.println("press 1 to contiue else 0 to exit");
            conChoice = scan.nextInt();
        }while(conChoice == 1);
        
    }
    class Convertor {
        private float inr;
        private float euro;
        private float yen;
        private float dollar;
        private int choice;
        public Convertor(float inr,float dollar,float euro,float yen,int choice) {
            inr = inr;
            dollar = dollar;
            euro = euro;
            yen = yen;
            choice = choice;
        }
        public void printInrCurrency() {
            switch(choice) {
                case 1:
                    System.out.println("The Doller Amount is"+this.dollar);
                    break;
                case 2:
                    System.out.println("The Euro Amount is"+this.euro);
                    break;
                case 3:
                    System.out.println("The Yen Amount is"+this.yen);
            }
            System.out.println("The Inr Amount is"+this.inr);
        }
        
        public void printDollarCurrency() {
            switch(choice) {
                case 1:
                    System.out.println("The Inr Amount is"+this.inr);
                    break;
                case 2:
                    System.out.println("The Euro Amount is"+this.euro);
                    break;
                case 3:
                    System.out.println("The Yen Amount is"+this.yen);
            }
            System.out.println("The Doller Amount is"+this.dollar);
        }
        
        public void printYenCurrency() {
            switch(choice) {
                case 1:
                    System.out.println("The Dollar Amount is"+this.dollar);
                    break;
                case 2:
                    System.out.println("The Euro Amount is"+this.euro);
                    break;
                case 3:
                    System.out.println("The Inr Amount is"+this.inr);
            }
            System.out.println("The Yen Amount is"+this.yen);
        }
        public void printEuroCurrency() {
            switch(choice) {
                case 1:
                    System.out.println("The Inr Amount is"+this.inr);
                    break;
                case 2:
                    System.out.println("The Dollar Amount is"+this.dollar);
                    break;
                case 3:
                    System.out.println("The Yen Amount is"+this.yen);
            }
            System.out.println("The Euro Amount is"+this.euro);
        }
        public void inrConvertor() {
            Scanner scan=new Scanner(System.in);
            System.out.println("1.USD to INR 2.Euro to INR 3.Yen to INR");
            System.out.println("Enter the choice");
            choice = scan.nextInt();
            switch(choice) {
                case 1:
                    System.out.println("Enter the Dollar");
                    dollar = scan.nextFloat();
                    this.inr = dollar * 74.85f;
                    break;
                case 2:
                    System.out.println("Enter the euro");
                   euro = scan.nextFloat();
                    this.inr = euro * 88.66f;
                    break;
                case 3:
                    System.out.println("Enter the yen");
                    yen = scan.nextFloat();
                    this.inr = yen * 0.70f;
                    break;
                default:
                    System.out.println("Enter the correct choice");
            }
        }
        public void dollarConvertor(){
            Scanner scan=new Scanner(System.in);
            System.out.println("1.INR to Dollar 2.Euro to Dollar 3.Yen to Dollar");
            System.out.println("Enter the choice");
            choice = scan.nextInt();
            switch(choice) {
                case 1:
                    System.out.println("Enter the inr");
                    inr = scan.nextFloat();
                    this.dollar = inr * 0.013f;
                    break;
                case 2:
                    System.out.println("Enter the euro");
                   euro = scan.nextFloat();
                    this.dollar = euro * 1.15f;
                    break;
                case 3:
                    System.out.println("Enter the yen");
                    yen = scan.nextFloat();
                    this.inr = yen * 0.0094f;
                    break;
                default:
                    System.out.println("Enter the correct choice");
            }
        }
        public void euroConvertor(){
            Scanner scan=new Scanner(System.in);
            System.out.println("1.INR to Euro 2.Dollar to Euro 3.Yen to Euro");
            System.out.println("Enter the choice");
            choice = scan.nextInt();
            switch(choice) {
                case 1:
                    System.out.println("Enter the INR");
                    inr = scan.nextFloat();
                    this.euro = inr * 0.011f;
                    break;
                case 2:
                    System.out.println("Enter the Dollar");
                   dollar = scan.nextFloat();
                    this.euro = dollar * 0.84f;
                    break;
                case 3:
                    System.out.println("Enter the yen");
                    yen = scan.nextFloat();
                    this.euro = yen * 0.0079f;
                    break;
                default:
                    System.out.println("Enter the correct choice");
            }
        }
        public void yenConvertor(){
            Scanner scan=new Scanner(System.in);
            System.out.println("1.Dollar to Yen 2.Euro to Yen 3.INR to Yen");
            System.out.println("Enter the choice");
            choice = scan.nextInt();
            switch(choice) {
                case 1:
                    System.out.println("Enter the Dollar");
                    dollar = scan.nextFloat();
                    yen = dollar * 106.56f;
                    break;
                case 2:
                    System.out.println("Enter the euro");
                    euro = scan.nextFloat();
                    yen = euro * 126.21f;
                    break;
                case 3:
                    System.out.println("Enter the INR");
                    inr = scan.nextFloat();
                    yen = inr * 1.42f;
                    break;
                default:
                    System.out.println("Enter the correct choice");
            }
        }
    }
    public static void main(String[] args) {
        Scanner scan=new Scanner(System.in);
        int converterChoice = 1;
        do {
            System.out.println("1.CurrencyConverter 2.DistanceConverter 3.TimeConverter");
            int n=scan.nextInt();
            switch(n) {
                case 1:
                    CurrencyConverter cconv = new CurrencyConverter();
                    cconv.callClass();
                    break;
                case 2:
                    System.out.println("Distance Converter");
                    CurrencyConverter distconv = new CurrencyConverter();
                    distconv.KMcoverter();
                    distconv.printDistanceConverter();
                    break;
                case 3:
                    System.out.println("Time Converter");
                    Time timeconv=new Time(0,0,0);
                    timeconv.Timecoverter();
                    timeconv.printTimeConverter();
                    break;
                default:
                    System.out.println("Enter the given option");
                    continue;
            }
            System.out.println("press 1 to contiue to choose the converter else 0 to exit");
            converterChoice = scan.nextInt();
        }while(converterChoice == 1);
    }
}
