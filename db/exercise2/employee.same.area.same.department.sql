SELECT employee.first_name
      ,department.name AS department
      ,employee.area 
  FROM employee employee
     ,department department 
WHERE employee.department_id=department.id 
  AND employee.area IN('coimbatore','tirpur') 
  AND department.name IN('Recruitment','HR') 
ORDER BY department.name;