Requirement 
1.To implement doPut method for Person Service

Entity
1.PersonServlet

Method Signature
1.public void doPut(HttpServletRequest req, HttpServletResponse res) throws Exception ;

Jobs to be Done
1.Get the person JSON from servlet request and store it in personJSON
2.Perpare the jsonUtil to JsonUtil
3.Parse the personJson to Person Object called personUpdate
4.Prepare the PersonService object called personService
5.invoke the update method from the personService
6.Pass the personUpdate Object to the update method.
7.Parse the response to the HttpsServletResponse body.

Pseudo Code

public class personServlet {
    
    public void doPut(HttpServletRequest req, HttpServletResponse res) throws Exception {
        Person person = new Person(req.getParameter("firstName"),
                                   req.getParameter("lastName"),
                                   req.getParameter("email"),
                                   req.getParameter("dob");
        PersonService personService = new PersonService();
        
        personService.update(person);
        
        res.setValue(person);
    }
}