SELECT MIN(age)
  FROM staff;

SELECT MAX(age)
  FROM staff;

SELECT AVG(age)
  FROM staff;

SELECT COUNT(age)
  FROM staff;
  
SELECT AVG(salary)
  FROM staff ;  
  
SELECT name AS Student_name
      ,year_of_joining
  FROM student
  ORDER BY year_of_joining ASC LIMIT 1 ;

SELECT name AS Student_name
      ,year_of_joining
  FROM student
  ORDER BY year_of_joining DESC LIMIT 1 ;  