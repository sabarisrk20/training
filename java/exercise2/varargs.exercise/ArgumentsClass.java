/*
    Problem Statement
    1.Demonstrate Overloading with varArgs
    
    Entity
    1.varArgs Class
    2.methodOne Method with varArg Argument
    
    Work Done
    1.Creating the method with varArg argument
    2.Overridding the method with Different varArg Argument
    3.Calling the method.
*/

public class ArgumentsClass {
    //varArgs Method//
    public static void methodOne(int... numbers) {
        for(int i:numbers) {
            System.out.println(i);
        }
    }
    
    //Overloading varArgs Method//
    public static void methodOne(String... names) {
        for(String o:names) {
            System.out.println("The Value is "+o);
        }
    }
    
    public static void methodOne(Boolean... flags) {
        for(Boolean b:flags) {
            System.out.println("Flag "+b);
        }
    }
    
    public static void main(String[] args) {
        methodOne(89,12,43,67);//varArg method with int Argument//
        methodOne("Shree","Sanjay");//varArg method with String Argument//
        methodOne(true,false,true,true);//varArg method with Boolean Argument//
    }
}