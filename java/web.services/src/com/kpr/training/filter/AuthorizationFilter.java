package com.kpr.training.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class AuthorizationFilter implements Filter{
	
	private FilterConfig filter = null;

	@Override
	public void destroy() {
		filter = null;
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		chain.doFilter(req, res);
	}

	@Override
	public void init(FilterConfig filter) throws ServletException {
		this.filter = filter;
	}

}
