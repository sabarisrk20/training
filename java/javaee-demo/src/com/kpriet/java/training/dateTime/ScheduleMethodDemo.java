package com.kpriet.java.training.dateTime;

/* 
Requirement:
     write a Java program to demonstrate schedule method 
Entity:
    ScheduleMethodDemo

Function Declaration:
    public static void main(String[] args)
    public void run()
    
Jobs to be done:
    1. Reference is created for a Timer class and also the reference is created for TimerTask.
    2. Inside TimerTask run method is invoked 
       2.1) Inside run method ,loop to print numbers is declared.
       2.2) To avoid repetition timer.cancel method is invoked
       2.3) To know the cancelled task purge method is used 
       2.4) Using Schedule method task is repeated and 
            it display the output in milliseconds delay which is declared inside the schedule method
        
Pseudo code:

public class ScheduleMethodDemo {
    
    public static void main(String[] args) {

        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            
            public void run() {
                
                for (int initialValue = 0; initialValue <= 10; initialValue++ ) {
                    System.out.println( "number : " + initialValue);
                    timer.cancel();
                    System.out.println("stop" + " purge value of Task : " + timer.purge());
                }   
            }
        };
        
        timer.schedule(task,1000,1);
    }
}
*/
import java.util.Timer;
import java.util.TimerTask;

public class ScheduleMethodDemo {
    public static void main(String[] args) {
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            public void run() {
                for(int initialValue = 0; initialValue <= 10; initialValue++ ) {
                    System.out.println( "number : " + initialValue);
                    timer.cancel();
                } 
                System.out.println("stop" + " purge value of Task : " + timer.purge());
            }
        };
        timer.schedule(task,100000,10000); // timer schedule( task,first date ,delay)
       //timer.scheduleAtFixedRate(task,500,1000); //scheduleAtFixedRate( TimerTask task ,long delay ,long period)
       //timer.scheduleAtFixedRate(task,new Date(),1000);// scheduleAtFixedRate( TimerTask task, Date firstTime, long period)
    }
}

