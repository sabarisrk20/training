SELECT semester
      ,amount
      ,paid_status
      ,roll_no
      ,stud_name
      ,gender
      ,stud_email
      ,stud_phone
      ,college_name
      ,university_name
  FROM semester_fee semester_fee
  LEFT JOIN student student
    ON semester_fee.stud_id = student.stud_id
  LEFT JOIN college college
    ON student.college_id = college.college_id
 INNER JOIN university university
    ON college.univ_code = university.univ_code
 WHERE semester_fee.paid_status = 'PAID'
 ORDER BY student.stud_id;
 
SELECT semester
      ,amount
      ,paid_status
      ,roll_no
      ,stud_name
      ,gender
      ,stud_email
      ,stud_phone
      ,college_name
      ,university_name
  FROM semester_fee semester_fee
  LEFT JOIN student student
    ON semester_fee.stud_id = student.stud_id
  LEFT JOIN college college
    ON student.college_id = college.college_id
 INNER JOIN university university
    ON college.univ_code = university.univ_code
 WHERE semester_fee.paid_status = 'UNPAID'
 ORDER BY student.stud_id; 
 
 SELECT college_name
      ,university_name
      ,semester
      ,paid_year
      ,paid_status
      ,SUM(amount)
  FROM college_department coll_dept
 INNER JOIN semester_fee sem_fee
    ON coll_dept.cdept_id = sem_fee.cdept_id
 INNER JOIN college coll
    ON coll_dept.college_id = coll.college_id
 INNER JOIN university univ
    ON coll.univ_code = univ.univ_code
 WHERE paid_status = 'PAID'
   AND paid_year = 2020;