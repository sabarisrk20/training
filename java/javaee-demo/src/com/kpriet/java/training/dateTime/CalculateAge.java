package com.kpriet.java.training.dateTime;

/**
*Problem Statement
*4. Write a Java program to calculate your age 
*
*Requirement :
*    - Java program to calculate your age 
*
*Entity:
*    - CalculateAge
*
*Method Declaration:
*    - public static void main(String[] args)*
*
*Jobs to be done :
*    1.Invoke the LocalDate class and pass my date of birth in of LocalDate class method.
*    2.Invoke the LocalDate class and get current time using now method.
*    3.Inoke Period class pass dateOfBirth and now parameters in between method
*    4.Get the my age using Period object with getYears, getMonths and getDays method
*      
*Pseudo Code:
*''''''''''''
* 
*public class CalculateAge {  
*   public static void main(String[] args)
*    {
*        LocalDate dateOfBirth = LocalDate.of(2000, 9, 16);
*        LocalDate now = LocalDate.now();
*        Period difference = Period.between(dateOfBirth, now);
*     System.out.printf("My age %d , %d months and %d days old.\n\n", 
*                    difference.getYears(), difference.getMonths(), difference.getDays());
*   }
*}
*
*
**/

import java.time.LocalDate;
import java.time.Period;
import java.util.Scanner;

public class CalculateAge {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter your dob \"DD-MM-YYYY\"");
		int date = scanner.nextInt();
		int month = scanner.nextInt();
		int year = scanner.nextInt();
		// date of birth
		LocalDate dateOfBirth = LocalDate.of(year, month, date);
		// current date
		LocalDate now = LocalDate.now();
		// difference between current date and date of birth
		Period difference = Period.between(dateOfBirth, now);

		System.out.printf("My age %d , %d months and %d days old.\n\n", difference.getYears(), difference.getMonths(),
				difference.getDays());
		scanner.close();
	}
}
