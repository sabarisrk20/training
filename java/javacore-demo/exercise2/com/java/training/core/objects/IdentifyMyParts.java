package com.java.training.core.objects;

/*
   Problem Statement
   1.What are the class variables?
   2.What are the instance variables?
   
   Entites
   1.x
   2.y
   
   Work Done
   We are going to Find the Class Variables and 
   instance Variables and Manipulate them..
   
*/
public class IdentifyMyParts {
    public static int x = 7;
    public int y = 3;
    public static void main(String [] args) {
        
        //Class Variables are declared with static Signature//
        System.out.println("Class variables are x:"+x);
        
        IdentifyMyParts identify = new IdentifyMyParts();
        /*Instance Variables are declared with the instance of the
        particular Class and Called*/
        System.out.println("Instance Variable is y :"+identify.y);
        IdentifyMyParts a = new IdentifyMyParts();
        IdentifyMyParts b = new IdentifyMyParts();
        a.y = 5;
        b.y = 6;
        a.x = 1;
        b.x = 2;
        System.out.println("a.y = " + a.y); //Output: a.y = 5//
        System.out.println("b.y = " + b.y); //Output: b.y = 6//
        System.out.println("a.x = " + a.x); //Output: a.x = 2//
        System.out.println("b.x = " + b.x); //Output: b.x = 2//
        System.out.println("IdentifyMyParts.x = " + IdentifyMyParts.x); //IdentifyMyParts.x = 2//
    }
}