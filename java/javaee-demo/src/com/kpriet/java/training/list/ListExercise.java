package com.kpriet.java.training.list;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.Iterator;

/**
 * Problem Statement
 *  Create a list
 *    => Add 10 values in the list
 *    => Create another list and perform addAll() method with it
 *    => Find the index of some value with indexOf() and lastIndexOf()
 *    => Print the values in the list using 
 *        - For loop
 *        - For Each
 *        - Iterator
 *        - Stream API
 *    => Convert the list to a set
 *    => Convert the list to a array
 *  Explain about contains(), subList(), retainAll() and give example
 *  
 * Entity
 * 1.ListExercise Class
 * 
 * Work Done
 * 1.Created a ArrayList called firstList and added 10 Values as instructed.
 * 2.Created another ArrayList called secList and added some values to it.
 * 3.Now using the Method called addAll() to add the items in the two lists
 * 4.Now using anther Method called indexOf(),this method here used to find the item present in the list at the 
 *   index value passed as a parameter in the indexOf method..
 * 5.Now using Method called lastIndexOf(),this method here can find the last occurrence of the value provided,\
 *   Here the lastindexOf method find the last occurrence of the value provided to the method..
 * 6.Next Iterating the ArrayList using different loops such as for loop, for each, Iterator Class, Stream API
 * 7.First using the standard for loop to iterate the items in the list and printed the list items to console.
 * 8.Then using the for each loop and accessing the items and printing the items.
 * 9.Using the Iterator Class to Iterate the List items, Here created a Iterator object called 'it' and provided the 
 *   list to iterate, and by using while loop and using the hasNext() function to access the next item of the list and 
 *   printing the items using the next() function..
 * 10.Created another ArrayList numValue of Integer type.
 * 11.Using the Stream API to Iterate the List, Stream has the feature to join the function that are normally used.
 *    Here using the for each loop cordially with stream and accessing each items of the numValue List..  
 *     
 * 12.Next converting the List to Set using Stream API, create another Set called newSet of Integer type to store the List values 
 *    converted to Set.
 * 13.By passing the Stream function followed by collect function and passing Collectors.toSet method to convert the List
 *    to Set and stored in newSet Set and printed
 * 14.Now to convert the list to array by using toArray function, creating a new Integer Array to store the values and 
 *    passing the existing list and followed by the toArray method to convert the list to Array and printed it.
 * 15.Here using the contains method to find whether the value is present in the list or not, if the value is present then Exists  
 *    is printed else not exist is printed..
 * 16.At the Next Line using the subList() method to access the values from a starting point and a ending point in a list.
 *    Created another List of Integer type called subLists and storing the values present in the index of 2 to 5 and printed it.
 * 17.Next Created two lists called listOne and listTwo and added values. Here by using retainAll() method, the values
 *    which are similar in both the lists and printed in tte next Line.
 * 
 * 
 *
 */



public class ListExercise {
	
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		//First List
		List<String> firstList = new ArrayList<String>();
		
		firstList.add("Apple");
		firstList.add("Samsung");
		firstList.add("Blackberry");
		firstList.add("Oppo");
		firstList.add("Vivo");
		firstList.add("Sony");
		firstList.add("Micromax");
		firstList.add("Lava");
		firstList.add("Moto");
		firstList.add("One Plus");
		firstList.add("One Plus");
		
		//Second List
		List<String> secList = new ArrayList<String>();
		
		secList.add("Rog");
		secList.add("Samsung Fold");
		secList.add("Ipad");
		secList.add("Tablet");
		secList.add("Macbook Air");
		secList.add("Vivobook");
		
		//addAll method
		firstList.addAll(secList);
		
		
		//IndexOf() and lastIndexOf()
		System.out.println("Enter the Value to Find");
		String valueIn = scan.nextLine();
		System.out.println(firstList.indexOf(valueIn));
		
		//lastIndexOf()
		System.out.println("Enter the Duplicate value to find the last occurance");
		String dupliValue = scan.nextLine();
		System.out.println(firstList.lastIndexOf(dupliValue));
		
		
		
		//Printing the Values
		//For Loop
		System.out.println("Iterating using For Loop");
		for(int i =0;i<firstList.size();i++) {
			System.out.println(firstList.get(i));
		}
		
		
		//For each loop
		System.out.println("Iterating using For each loop");
		
		for(String val:secList) {
			System.out.println(val);
		}
		
		
		//Using Iterator
		System.out.println("Iterating using Iterator");
		
		Iterator it = secList.iterator();
		
		while(it.hasNext()) {
			System.out.println(it.next());
		}
		
		
		
		//Using Stream API
		
		List<Integer> numValue = new ArrayList<Integer>();
		numValue.add(1);
		numValue.add(14);
		numValue.add(18);
		numValue.add(11);
		numValue.add(11);
		numValue.add(10);
		
		System.out.println(numValue);
		
		List<Integer> marks = numValue.stream().map(x->x+x).collect(Collectors.toList());
		System.out.println(marks);
		
		List<Integer> findValue = numValue.stream().filter(s->s.equals(99)).collect(Collectors.toList());
		
		System.out.println(findValue);
		
		//Iterating using Stream API
		
		numValue.stream().forEach(y->System.out.println(y));
		
		//List to Set
		System.out.println("List to Set");
		Set<Integer> newSet = numValue.stream().collect(Collectors.toSet());
		
		for(Integer a: newSet) {
			System.out.println(a);
		}
		
		//List to Array
		System.out.println("List to Array");
		Integer[] intArr = numValue.toArray(new Integer[0]);
		
		for(Integer f : intArr) {
			System.out.println(f);
		}
		
		// Contains(), subList(), retainAll() Methods
		
		//Contains
		if(numValue.contains(11)) {
			System.out.println("11 Exists");
		} else {
			System.out.println("11 does not Exists");
		}
		//subList
		List<Integer> subLists = numValue.subList(2, 5);
		System.out.println(subLists);
		
		
		//retainAll
		
		List<String> listOne = new ArrayList<String>();
		listOne.add("Orange");
		listOne.add("Apple");
		listOne.add("Muskmelon");
		
		List<String> listTwo = new ArrayList<String>();
		listTwo.add("Apple");
		listTwo.add("Grape");
		listTwo.add("Orange");
		listTwo.add("JackFruit");
		
		listTwo.retainAll(listOne);
		
		listTwo.stream().forEach(s->System.out.println(s));
		//scan.close();
		
	}

}
