SELECT student.stu_id AS STUDENT_ID
      ,roll_no AS ROLL_NO
      ,stu_name AS STUDENT_NAME
      ,gender AS GENDER
      ,semester AS SEMESTER
      ,grade AS GRADE
      ,credits AS CREDITS
      ,GPA
  FROM student student
  LEFT JOIN semester_result semester_result
    ON student.stu_id = semester_result.stud_id
 WHERE GPA >8
   AND semester IN (4,6,8)
 ORDER BY stud_id;
 SELECT student.stu_id AS STUDENT_ID
      ,roll_no AS ROLL_NO
      ,stu_name AS STUDENT_NAME
      ,gender AS GENDER
      ,semester AS SEMESTER
      ,grade AS GRADE
      ,credits AS CREDITS
      ,GPA
  FROM student student
  LEFT JOIN semester_result semester_result
    ON student.stu_id = semester_result.stud_id
 WHERE GPA >5
   AND semester IN (4,6,8)
 ORDER BY stud_id;