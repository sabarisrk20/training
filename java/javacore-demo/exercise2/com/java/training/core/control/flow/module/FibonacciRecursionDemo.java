package com.java.training.core.control.flow.module;

public class FibonacciRecursionDemo {
    int num1=0,num2=1,temp;
    public void fiboRec(int num3) {
        if(num3>0) {
            System.out.print(num1 + " ");
            temp = num1 + num2;
            num1 = num2;
            num2 = temp;
            fiboRec(num3-1);
        }
    }
}