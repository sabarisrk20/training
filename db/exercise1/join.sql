SELECT student.name
      ,staff.name
      ,staff.subject
  FROM student student
  CROSS JOIN staff staff;

SELECT student.name
      ,staff.name
	  ,staff.subject
  FROM student student
  INNER JOIN staff staff
  ON student.staff_id = staff.id;  
  
SELECT student.name
      ,staff.name
	  ,staff.subject
  FROM student student
  LEFT JOIN staff staff
  ON student.staff_id = staff.id;
  
SELECT student.name
      ,staff.name
	  ,staff.subject
  FROM student student
  RIGHT JOIN staff staff
  ON student.staff_id = staff.id;