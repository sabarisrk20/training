CREATE TABLE `college` (
  `college_id` int NOT NULL,
  `college_code` varchar(4) NOT NULL,
  `college_name` varchar(100) NOT NULL,
  `univ_code` varchar(4) DEFAULT NULL,
  `city` varchar(45) NOT NULL,
  `state` varchar(45) NOT NULL,
  `year_opened` year NOT NULL,
  PRIMARY KEY (`college_id`),
  KEY `fk_univ_code_idx` (`univ_code`),
  CONSTRAINT `univ_code` FOREIGN KEY (`univ_code`) REFERENCES `university` (`univ_code`)
);

CREATE TABLE `college_department` (
  `cdept_id` int NOT NULL,
  `udept_code` varchar(4) DEFAULT NULL,
  `college_id` int DEFAULT NULL,
  PRIMARY KEY (`cdept_id`),
  KEY `fk_college_id_idx` (`college_id`),
  KEY `fk_udept_code_idx` (`udept_code`),
  CONSTRAINT `fk_college_id` FOREIGN KEY (`college_id`) REFERENCES `college` (`college_id`),
  CONSTRAINT `fk_udept_code` FOREIGN KEY (`udept_code`) REFERENCES `department` (`dept_code`)
);

CREATE TABLE `department` (
  `dept_code` varchar(4) NOT NULL,
  `dept_name` varchar(45) NOT NULL,
  `univ_code` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`dept_code`),
  KEY `fk_univ_code_idx` (`univ_code`),
  CONSTRAINT `fk_univ_code` FOREIGN KEY (`univ_code`) REFERENCES `university` (`univ_code`)
);

CREATE TABLE `designation` (
  `desig_id` int NOT NULL,
  `desig_name` varchar(45) NOT NULL,
  `desig_rank` varchar(45) NOT NULL,
  PRIMARY KEY (`desig_id`)
);

CREATE TABLE `employee` (
  `id` int NOT NULL,
  `emp_name` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `email` varchar(45) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `college_id` int DEFAULT NULL,
  `cdept_id_emp` int DEFAULT NULL,
  `desig_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_college_id_idx` (`college_id`),
  KEY `fk_college_id_emp_idx` (`college_id`),
  KEY `fk_cdept_id_emp_idx` (`cdept_id_emp`),
  KEY `fk_desig_id_emp_idx` (`desig_id`),
  CONSTRAINT `fk_cdept_id_emp` FOREIGN KEY (`cdept_id_emp`) REFERENCES `college_department` (`cdept_id`),
  CONSTRAINT `fk_college_id_emp` FOREIGN KEY (`college_id`) REFERENCES `college` (`college_id`),
  CONSTRAINT `fk_desig_id_emp` FOREIGN KEY (`desig_id`) REFERENCES `designation` (`desig_id`)
);

CREATE TABLE `university` (
  `univ_code` varchar(4) NOT NULL,
  `university_name` varchar(100) NOT NULL,
  PRIMARY KEY (`univ_code`)
);

CREATE TABLE `syllabus` (
  `id` int NOT NULL,
  `cdept_id` int DEFAULT NULL,
  `syllabus_code` varchar(4) NOT NULL,
  `syllabus_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cdept_id_idx` (`cdept_id`),
  CONSTRAINT `fk_cdept_id` FOREIGN KEY (`cdept_id`) REFERENCES `college_department` (`cdept_id`)
);

CREATE TABLE `student` (
  `stu_id` int NOT NULL,
  `roll_no` varchar(8) NOT NULL,
  `stu_name` varchar(45) NOT NULL,
  `stu_dob` date NOT NULL,
  `gender` varchar(2) NOT NULL,
  `stu_email` varchar(50) NOT NULL,
  `stu_phone` bigint NOT NULL,
  `stu_address` varchar(200) NOT NULL,
  `academic_year` varchar(4) NOT NULL,
  `cdept_id` int NOT NULL,
  `college_id` int DEFAULT NULL,
  PRIMARY KEY (`stu_id`),
  KEY `fk_college_id_idx` (`college_id`),
  KEY `college_id_stu_idx` (`college_id`),
  KEY `fk_cdept_id_stu_idx` (`cdept_id`),
  CONSTRAINT `college_id_stu` FOREIGN KEY (`college_id`) REFERENCES `college` (`college_id`),
  CONSTRAINT `fk_cdept_id_stu` FOREIGN KEY (`cdept_id`) REFERENCES `college_department` (`cdept_id`)
);

CREATE TABLE `semester_result` (
  `stud_id` int NOT NULL,
  `syllabus_id` int DEFAULT NULL,
  `semester` varchar(45) NOT NULL,
  `grade` varchar(3) NOT NULL,
  `credits` float NOT NULL,
  `result_date` date NOT NULL,
  `GPA` decimal(10,0) NOT NULL,
  PRIMARY KEY (`stud_id`),
  KEY `fk_syllabus_id_semr_idx` (`syllabus_id`),
  CONSTRAINT `fk_stud_id_semr` FOREIGN KEY (`stud_id`) REFERENCES `student` (`stu_id`),
  CONSTRAINT `fk_syllabus_id_semr` FOREIGN KEY (`syllabus_id`) REFERENCES `syllabus` (`id`)
);

CREATE TABLE `semester_fee` (
  `cdept_id` int DEFAULT NULL,
  `stud_id` int NOT NULL,
  `semester` tinyint NOT NULL,
  `amount` varchar(45) DEFAULT NULL,
  `paid_year` year DEFAULT NULL,
  `paid_status` varchar(10) NOT NULL,
  PRIMARY KEY (`stud_id`),
  KEY `fk_cdept_id_sem_idx` (`cdept_id`),
  KEY `fk_student_id_sem_idx` (`stud_id`),
  CONSTRAINT `fk_student_id_sem` FOREIGN KEY (`stud_id`) REFERENCES `student` (`stu_id`)
);

CREATE TABLE `professor_syllabus` (
  `emp_id` int DEFAULT NULL,
  `syllabus_id` int NOT NULL,
  `semester` tinyint NOT NULL,
  PRIMARY KEY (`syllabus_id`),
  KEY `fk_syllubus_id_idx` (`syllabus_id`),
  KEY `fk_emp_id_idx` (`emp_id`),
  CONSTRAINT `fk_emp_id` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`id`),
  CONSTRAINT `fk_syllubus_id` FOREIGN KEY (`syllabus_id`) REFERENCES `syllabus` (`id`)
);