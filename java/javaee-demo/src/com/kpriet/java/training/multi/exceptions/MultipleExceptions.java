package com.kpriet.java.training.multi.exceptions;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/**
 * Problem Statement
 * 1.Explain Multiple Catch Block with Example
 * 2.It is possible to have more than one try block? - Reason
 * 3.Difference between catching multiple exceptions and Mutiple catch blocks
 * 
 * Requirement
 * 1.Explain Multiple Catch Block with Example
 * 2.It is possible to have more than one try block? - Reason
 * 3.Difference between catching multiple exceptions and Mutiple catch blocks
 * 
 * Method Signature
 * 
 * 
 * Entity
 * 1.MultipleExceptions 
 * 
 * Jobs to be Done
 * 1.Create a try catch block and got a value from user using Scanner Class
 * 2.This try Block catches two Exceptions such as IOException and ArithmeticException
 * 3.One Exception is caught using throws keyword and another is caught using catch Block.
 * 4.Here the Arithmetic Exception is caught only the total count of the user entered value is below 0.
 *   it is caught by using throw new keyword and followed by the exception name.
 *   
 * Answers:
 * 2.No it is not possible to have multiple try block in Java since it will throw Compile time error.
 * 3.Multi catch vs Multiple Catch
 *   1.Multiple catch Feature does not give Resource Management Feature.
 *     Multi catch Feature gives the Resource Management Feature. 
 *   2.Syntax of Multiple catch uses multiple catch blocks and multi catch block uses | Operator to separate two exceptions
 *   
 * Pseudo Code
 * class MultipleExceptions {
 *	
 *	public static void main(String[] args) throws IOException {
 *		
 *		try {
 *			Scanner scan = new Scanner(System.in);
 *			System.out.println("Enter some Values");
 *			String inValue = scan.nextLine();
 *			int tot = inValue.length();
 *			if(tot <= 0) {
 *				throw new ArithmeticException();
 *			}
 *			else {
 *				System.out.println(tot);
 *			}
 *		} catch (ArithmeticException ae) {
 *	 		System.out.println("Enter a Value more than a blank space");
 *		}
 *	}
 *
 *}
 *
 *
 *
 */

public class MultipleExceptions {
	
	public static void main(String[] args) throws IOException {
		
		try {
			Scanner scan = new Scanner(System.in);
			System.out.println("Enter some Values");
			String inValue = scan.nextLine();
			int tot = inValue.length();
			if(tot <= 0) {
				throw new ArithmeticException();
			}
			else {
				System.out.println(tot);
			}
		} catch (ArithmeticException ae) {
			System.out.println("Enter a Value more than a blank space");
		}
	}

}