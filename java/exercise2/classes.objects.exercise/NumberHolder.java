/*
    Problem Statement
    1.Write Code to create Instant of a Class and initialize
      the members values and display the values.
      
    Entites
    1.NumberHolder Class
    2.anInt
    3.aFloat
    
    Work Done
    Creating an Object for a Class and initializing the values 
    and displaying the member variables.
*/

public class NumberHolder {
        public int anInt;
        public float aFloat;
        
        public static void main(String[] args) {
            NumberHolder numberholder = new NumberHolder();
            numberholder.anInt = 10;
            numberholder.aFloat = 7;
            System.out.println(numberholder.anInt);//Output:10//
            System.out.println(numberholder.aFloat);//Output:7.0//
        }
    }