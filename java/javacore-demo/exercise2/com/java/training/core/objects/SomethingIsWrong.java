package com.java.training.core.objects;
/*
    Problem Statement
    Debugging the Code..

    Entites
    SomethingIsWrong Class
    
    Work Done
    1.Since the public Class has a Rectangle Class incomplete
      object created
    2.Creating a Rectangle class 
    3.Declaring two variables as
      height and width
      and public method area to return area of rectangle
    4.Creating the Object for the Rectangle as myRect
    5.Since it was Half way completed it throws as variable
      initializing Error.
    
*/

class Rectangle {
    int width;
    int height;
    
    public int area() {
        return height*width;
    }
}

public class SomethingIsWrong {
    public static void main(String[] args) {
        Rectangle myRect = new Rectangle();
        myRect.width = 40;
        myRect.height = 50;
        System.out.println("myRect's area is " + myRect.area());
        //Output : myRect's area is 2000//
    }
}