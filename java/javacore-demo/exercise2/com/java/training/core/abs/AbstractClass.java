package com.java.training.core.abs;

/*
    Problem Statement
    1.Demonstrate abstract classes using Shape class.
    2.Shape class should have methods to calculate area and perimeter
    3.Define two more class Circle and Square by extending Shape class
      and implement the calculation for each class respectively
      
    Entity
    1.Shape
    2.Circle
    3.Rectangle
    
    Work Done
    1.Created Abstract Shape class with area Calculating method as areaCalc
      and Parameter Calculating method as periCalc for both circle and rectangle
    2.Creating Circle class and implementing the area and perimeter calculating methods
      since we are extending abstract class we need to implement all the methods.
    3.Creaing Rectangle class and implementing the are and parameter calculating
      methods.
    4.Creating Instance for the Circle and Rectangle class and calling the area and 
      perimeter methods and passing the parameter to calculate..
*/

import java.util.*;
abstract class Shape {
    abstract public double printArea();
    abstract public double printPerimeter();
}
class Square extends Shape {
    private double side;
    public Square(double side) {
        this.side= side;
    }
    public double printArea() {
        return side * side;
    }
    public double printPerimeter() {
        return 4 * side;
    }
}

class Circle extends Shape {
    private double radius;
    public Circle(double radius) {
        this.radius = radius;
    }
    public double printArea() {
        return (3.14f * (radius * radius));
    }
    public double printPerimeter() {
        return 2 * 3.14f * radius;
    }
}

public class AbstractClass
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);   
        System.out.println("Choose the Shape");
        System.out.println("1. Square 2.Circle");
        int cho = scan.nextInt();
        switch(cho)
        {
            case 1:
                System.out.println("Enter the length");
                int side = scan.nextInt();
                Square square =new Square(side);
                System.out.println("The Area of the Square is "+ square.printArea());
                System.out.println("The Perimeter of the Square is "+ square.printPerimeter());
                break;
            case 2:
                System.out.println("Enter the radius");
                int radius = scan.nextInt(); 
                Circle circle =new Circle(radius);          
                System.out.println("The Area of the Circle is "+ circle.printArea());
                System.out.println("ThePerimeter of the Circle is "+ circle.printPerimeter());
                break;
        }    
    }
}