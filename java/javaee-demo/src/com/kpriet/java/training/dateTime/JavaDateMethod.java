package com.kpriet.java.training.dateTime;

import java.util.Date; 

public class JavaDateMethod {
    
    @SuppressWarnings("deprecation")
    public static void main(String[] args) {  
        Date date1 = new Date(2020,9,21); //Date()
        Date date2 = new Date(1999,3,11);  //Date(845686L)
        System.out.println("Date 'date1' is after Date 'date2' : " + date1.after(date2));
        
        // check whether date1 comes after date 2, if yes return true      
        System.out.println("Date 'date1' is after Date 'date2' : " + date1.before(date2));
        
         // check whether date1 comes before date 2 , if yes return  false      
        System.out.println("Clone of your Date 'date1' : " + date1.clone());
        
        //It returns a clone/copy of this instance.
        int comparison = date1.compareTo(date2);
        
        //It returns the value 0 if the argument Date is equal to this Date.
        //It returns a value less than 0 (negative)if this Date is before the Date argument.
        //It returns a value greater than 0 (positive)if this Date is after the Date argument.(1)
        System.out.println("Your comparison value is : " + comparison);
        System.out.println("Date 'date1' equals Date 'date2' : " + date1.equals(date2)); 
        
        //It returns true if the dates are same otherwise false.
        System.out.println("Current number of milliseconds since January 1, 1970, 00:00:00 GTM : "
        + date1.getTime());
        
        //It returns the number of milliseconds since January 1, 1970, 00:00:00 GTM.
    }
}    