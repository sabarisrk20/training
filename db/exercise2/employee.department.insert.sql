INSERT INTO `database1`.`employee` (
            `id`
           ,`first_name`
           ,`surname` 
           ,`dob` 
           ,`date_of_joining`
           ,`annual_salary`
           ,`department_id`)
VALUES(1
     ,'ameer'
     ,'khan'
     ,'2001-02-17'
     ,'2019-06-14'
     ,'500000'
     ,1);
INSERT INTO `database1`.`employee` (
            `id`
            ,`first_name`
            ,`surname`
            ,`dob`
            ,`date_of_joining`
            ,`annual_salary`
            ,`department_id`)
VALUES (2
      ,'sabari'
      ,'ramkumar'
      ,'2000-12-20'
      ,'2017-02-16'
      ,'500000'
      ,2);
INSERT INTO `database1`.`employee` (
            `id`
            ,`first_name`
            ,`surname`
            ,`dob`
            ,`date_of_joining`
            ,`annual_salary`
            ,`department_id`) 
VALUES (3
      ,'seetha'
      ,'lakshmi'
      ,'2002-03-19'
      ,'2019-08-16'
      ,'600000'
      ,3);
INSERT INTO `database1`.`employee` (
            `id`
           ,`first_name`
           ,`surname`
           ,`dob`,
           `date_of_joining`
           ,`annual_salary`
           ,`department_id`) 
VALUES (4
      ,'koushika'
      ,NULL
      ,'2001-05-17'
      ,'2019-09-12'
      ,'400000'
      ,4);
INSERT INTO `database1`.`employee` (
            `id`
            ,`first_name`
            ,`surname`
            ,`dob`
            ,`date_of_joining`
            ,`annual_salary`
            ,`department_id`)
VALUES (5
      ,'rithanyaa'
      ,NULL
      ,'2002-05-12'
      ,'2017-09-18'
      ,'500000'
      ,5);
INSERT INTO `database1`.`employee` (
            `id`
            ,`first_name`
            ,`surname`
            ,`dob`
            ,`date_of_joining`
            ,`annual_salary`
            ,`department_id`)
VALUES (6
      ,'shree'
      ,'dhushandhan'
      ,'2001-09-13'
      ,'2019-05-14'
      ,'470000'
      ,6);
INSERT INTO `database1`.`employee` (
            `id`
           ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`department_id`) 
VALUES (7
      ,'dhivya'
      ,'shri'
      ,'2001-08-03'
      ,'2019-04-14'
      ,'430000'
      ,1);
INSERT INTO `database1`.`employee` (
            `id`
            ,`first_name`
            ,`surname`
            ,`dob`
            ,`date_of_joining`
            ,`annual_salary`
            ,`department_id`)
VALUES (8
      ,'saravanan'
      ,NULL
      ,'2003-08-05'
      ,'2019-05-04'
      ,'450000'
      ,2);
INSERT INTO `database1`.`employee` (
            `id`
           ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`department_id`)
VALUES (9
      ,'priya'
       ,NULL
      ,'2000-02-15'
      ,'2018-02-15'
      ,'560000'
      ,3);
INSERT INTO `database1`.`employee` (
            `id`
           ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`department_id`) 
VALUES (10
      ,'hariharasudhan'
      ,NULL
      ,'1999-12-12'
      ,'2019-06-19'
      ,'435000'
      ,4);
INSERT INTO `database1`.`employee` (
            `id`
           ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`department_id`) 
VALUES (11
      ,'myvizhi'
      ,NULL
      ,'2001-12-12'
      ,'2018-07-15'
      ,'456000'
      ,5);
INSERT INTO `database1`.`employee` (
            `id`
           ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`department_id`)
VALUES (12
      ,'preethi'
      ,NULL
      ,'2001-09-17'
      ,'2017-02-14'
      ,'340000'
      ,6);
INSERT INTO `database1`.`employee` (
            `id`
           ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`department_id`) 
VALUES (13
       ,'santheep'
       ,'c'
       ,'2001-03-16'
       ,'2018-07-18'
       ,'121000'
       ,1);
INSERT INTO `database1`.`employee` (
            `id`
            ,`first_name`
            ,`surname`
            ,`dob`
            ,`date_of_joining`
            ,`annual_salary`
            ,`department_id`) 
VALUES (14
      ,'sowmiya'
      ,NULL
      ,'1998-05-14'
      ,'2019-05-14'
      ,'605000'
      ,2);
INSERT INTO `database1`.`employee` (
            `id`
            ,`first_name`
            ,`surname`
            ,`dob`
            ,`date_of_joining`
            ,`annual_salary`
            ,`department_id`)
VALUES (15
        ,'suresh'
        ,'kumar'
        ,'2001-08-19'
        ,'2017-08-02'
        ,'242000'
        ,3);
INSERT INTO `database1`.`employee` (
            `id`
            ,`first_name`
            ,`surname`
            ,`dob`
            ,`date_of_joining`
            ,`annual_salary`
            ,`department_id`) 
VALUES (16
      ,'shibi'
      ,'selva'
      ,'2002-11-19'
      ,'2020-02-20'
      ,'363000'
      ,4);
INSERT INTO `database1`.`employee` (
            `id`
           ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`department_id`) 
VALUES (17
      ,'kavi'
      ,'priya'
      ,'1998-05-18'
      ,'2018-09-15'
      ,'121000'
      ,5);
INSERT INTO `database1`.`employee` (
            `id`
            ,`first_name`
            ,`surname`
            ,`dob`
            ,`date_of_joining`
            ,`annual_salary`
            ,`department_id`) 
VALUES (18
      ,'parameshwaren'
       ,NULL
       ,'2001-06-18'
       ,'2018-11-05'
       ,'242000'
       ,6);
INSERT INTO `database1`.`employee` (
            `id`
            ,`first_name`
            ,`surname`
            ,`dob`
            ,`date_of_joining`
            ,`annual_salary`
            ,`department_id`) 
VALUES (19
      ,'rudhran'
      ,NULL
      ,'2001-05-19'
      ,'2018-02-11'
      ,'605000'
      ,1);
INSERT INTO `database1`.`employee` (
            `id`
            ,`first_name`
            ,`surname`
            ,`dob`
            ,`date_of_joining`
            ,`annual_salary`
            ,`department_id`) 
VALUES (20
      ,'nivetha'
       ,NULL
      ,'2000-05-29'
      ,'2019-02-02'
      ,'726000'
      ,2);
INSERT INTO `database1`.`employee` (
            `id`
           ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`department_id`) 
VALUES (21
      ,'swetha'
       ,NULL
      ,'2001-02-14'
      ,'2019-03-03'
      ,'847000'
      ,3);
INSERT INTO `database1`.`employee` (
            `id`
            ,`first_name`
            ,`surname`
            ,`dob`
            ,`date_of_joining`
            ,`annual_salary`
            ,`department_id`) 
VALUES (22
      ,'santheesh'
       ,NULL
      ,'1997-07-19'
      ,'2019-04-04'
      ,'968000'
       ,4);
INSERT INTO `database1`.`employee` (
            `id`
           ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`department_id`) 
VALUES (23
      ,'mithaesh'
      ,NULL
      ,'1998-04-14'
      ,'2014-09-23'
      ,'968000'
      ,5);
INSERT INTO `database1`.`employee` (
            `id`
            ,`first_name`
            ,`surname`
            ,`dob`
            ,`date_of_joining`
            ,`annual_salary`
            ,`department_id`) 
VALUES (24
      ,'diya'
      ,NULL
      ,'2001-03-25'
      ,'2018-06-25'
      ,'145200'
      ,6);
INSERT INTO `database1`.`employee` (
            `id`
            ,`first_name`
            ,`surname`
            ,`dob`
            ,`date_of_joining`
            ,`annual_salary`
            ,`department_id`) 
VALUES (25
      ,'ruban'
      ,NULL
      ,'1995-10-29'
      ,'2018-09-16'
      ,'484000'
      ,1);
INSERT INTO `database1`.`employee` (
            `id`
            ,`first_name`
            ,`surname`
            ,`dob`
            ,`date_of_joining`
            ,`annual_salary`
            ,`department_id`) 
VALUES (26
      ,'kalai'
      ,'selvan'
      ,'1996-11-20'
      ,'2015-03-03'
      ,'605000'
      ,2);
INSERT INTO `database1`.`employee` (
            `id`
            ,`first_name`
            ,`surname`
            ,`dob`
            ,`date_of_joining`
            ,`annual_salary`
            ,`department_id`) 
VALUES (27
      ,'saran'
      ,'krithic'
      ,'2000-09-14'
      ,'2018-03-15'
      ,'726000'
      ,3);
INSERT INTO `database1`.`employee` (
            `id`
           ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`department_id`) 
VALUES (28
      ,'kavitha'
      ,NULL
      ,'2002-06-14'
      ,'2018-05-05'
      ,'847000',4);
INSERT INTO `database1`.`employee` (
            `id`
           ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`department_id`) 
VALUES (29
      ,'venkat'
      ,NULL
      ,'1995-02-17'
      ,'2018-02-01'
      ,'484000',5);
INSERT INTO `database1`.`employee` (
            `id`
           ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`department_id`) 
VALUES (30
      ,'ravi'
      ,'varma'
      ,'2000-05-20'
      ,'2018-01-02'
      ,'242000'
      ,6);

INSERT INTO `database1`.`department` (
            `id`
           ,`name`)
VALUES (1
      ,'ITDesk');
INSERT INTO `database1`.`department` (
            `id`
           ,`name`)
VALUES (2
      ,'Finance');
INSERT INTO `database1`.`department` (
            `id`
           ,`name`)
VALUES (3
      ,'Engineering');
INSERT INTO `database1`.`department` (
            `id`
           ,`name`)
VALUES (4
      ,'HR');
INSERT INTO `database1`.`department` (
            `id`
           ,`name`)
VALUES (5
      ,'Recruitment');
INSERT INTO `database1`.`department` (
            `id`
           ,`name`)
VALUES (6
      ,'Facility');

