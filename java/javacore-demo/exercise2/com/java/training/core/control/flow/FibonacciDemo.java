/*
    Problem Statement
    1.Complete Fibnocci Series with 
        for loop
        while loop
        Recursion
    
    Entites
    1.Total range of Fibnocci
    2.initial values a and b
    
    Work Done
    Sum the last two values to produce a Fibnocci Series.
     1)Used For Loop as a package as FibonacciForDemo.java and imported into the FibonacciDemo class
     2)Used While Loop as a package as FibonacciWhileDemo.java and imported into the FibonacciDemo class
     3)Used Recursion as a package as FibonacciRecursionDemo.java and imported into the FibonacciDemo class
*/
package com.java.training.core.control.flow;

import java.util.Scanner;
import com.java.training.core.control.flow.module.FibonacciForDemo;
import com.java.training.core.control.flow.module.FibonacciRecursionDemo;
import com.java.training.core.control.flow.module.FibonacciWhileDemo;

class FibonacciDemo {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int choice, choice1 = 1, num3;
        do {
            System.out.println("1.Fibonacci For Loop 2.Fibonacci While Loop 3.Fibonacci Recurssion");
            System.out.println("Enter your Choice");
            choice = scan.nextInt();
            switch(choice) {
                case 1:
                    System.out.println("Fibonacci For Loop");
                    System.out.println("Enter the number to find its fibonacci series:");
                    num3 = scan.nextInt();
                    FibonacciForDemo fibForDemo = new FibonacciForDemo();
                    fibForDemo.fiboFor(num3);
                    //System.out.println(" ");
                    break;
                case 2:
                    System.out.println("Fibonacci While Loop");
                    System.out.println("Enter the number to find its fibonacci series:");
                    num3 = scan.nextInt();
                    FibonacciWhileDemo fibWhileDemo = new FibonacciWhileDemo();
                    fibWhileDemo.fiboWhile(num3);
                    System.out.println("");
                    break;
                case 3:
                    System.out.println("Fibonacci Recurssion");
                    System.out.println("Enter the number to find its fibonacci series:");
                    num3 = scan.nextInt();
                    FibonacciRecursionDemo fibRecDemo = new FibonacciRecursionDemo();
                    fibRecDemo.fiboRec(num3);
                    System.out.println(" ");
                    break;
                default:
                    System.out.println("Enter the correct choice");
                    continue;
            }
            System.out.print("Press 1 to continue else 0 to exit : ");
            choice1 = scan.nextInt();
        }while(choice1 == 1);
   }
}