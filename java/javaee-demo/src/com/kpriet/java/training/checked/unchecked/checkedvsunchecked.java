package com.kpriet.java.training.checked.unchecked;
Checked vs Unchecked Exceptions

1.Checked Exceptions
	1.Checked Exceptions are caught during the Compile Time.
	2.These Exceptions must be thrown caught or thrown using the try and catch block or throws Keyword..
	3.Checked Exceptions are
		1.IOExceptions
		2.ArrayIndexOutOfBoundsExceptions
		3.FileNotFoundExceptions
		
2.Unchecked Exceptions
	1.Unchecked Exceptions are caught during the Runtime, these Exceptions are left as valid at the 
		Compile time and it gives error during the Runtime..
	2.These Exceptions are not caught or thrown while writing the Source Code.
	3.Unchecked Exceptions are 
		1.Arithmetic Exceptions
		2.NullPointerExceptions 
		
		
Can we write only try block without catch and finally blocks?why?

	1.When we use try Block without catch or finally block the source code won't execute and it
		throws a Unresolved Compilation problem in CompileTime.
	2.Either the try Block should have a catch Block or Finally Block.
	3.We can use try block without catch block but with finally block since the finally block always executes..  		
		
		