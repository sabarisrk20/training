package com.kpriet.java.training.properties;

/* 
Requirement:
    Write a program to perform the following operations in Properties.
    i)   Add some elements to the properties file.
    ii)  print all the elements in the properties file using iterator.
    iii) print all the elements in the properties file using list method.
      
Entity:
    PropertiesDemo
    
Function Declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1) Create a properties and declare object as property 
    2) Add elements to the property
    3) Create a object for FileOutputStream and assign a name for property file.
    4) Store the property in FileOutputStream as property file.
    5) Create an iterator for property file and store it in iterator.
    6) For each element in property
      6.1) Store the key in key
      6.2) Store the value in value
      6.3) Print the key and value.
    7) Print the property using list.
        
Pseudo code:
  
public class PropertiesDemo {

    public static void main(String[] args) throws IOException{
        Properties property = new Properties();
        
        //add elements to the property
         property.setProperty("1", "Java");

        OutputStream stream = new FileOutputStream("File.properties");
        property.store(stream , null);
        

        Iterator<Object> iterator = property.keySet().iterator();
        while (iterator.hasNext()) {
            String key = (String) iterator.next();
            String value = property.getProperty(key);
            System.out.println(key + " = " + value);
        }
        property.list(System.out);
    }
}
*/

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.Properties;

public class PropertiesDemo {

    public static void main(String[] args) throws IOException{
        Properties property = new Properties();
        property.setProperty("1", "Java");
        property.setProperty("2", "C++");
        property.setProperty("3", "C");
        property.setProperty("4", "Python");
        OutputStream stream = new FileOutputStream("File.properties");
        property.store(stream , null);
        System.out.println("Printing all the elements in the property file using iterator");
        Iterator<Object> iterator = property.keySet().iterator();
        while (iterator.hasNext()) {
            String key = (String) iterator.next();
            String value = property.getProperty(key);
            System.out.println(key + "  =  " + value);
        }
        property.list(System.out);
    }
}

