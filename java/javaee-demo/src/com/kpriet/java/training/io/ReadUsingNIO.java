/*
Requirement:
    Read a file using java.nio.Files using Paths

Entity:
    ReadUsingNIO

Function Declaration:
    public static void main(String[] args) 

Jobs to be done:
    1.Declare a class as mentioned below
    2.Under main method invoke the Path class and Paths class and get the path of the file
    3.Invoke the Charset Class and invoke forName method and 
      pass a canonical name or an alias and its respective charset name is returned.
    4.Inside try block Invoke a list and readAllLines method and pass the parameter as 
      object of path and charset's object
    5.Using for each read line by line and print it.
    6. If any error exception caused the catch block catch the exception and print it.
    
Pseudo code:

public class ReadUsingNIO {
    
    public static void main(String[] args) {
        Path demoPath // use path
        Charset charset // use charset
        try {
            List<String> lines = Files.readAllLines(demoPath, charset);
            for (String line : lines) {
               System.out.println(line);
            }
         } catch (IOException e) {
            System.out.println(e);
         }
    }
}
*/

package com.kpriet.java.training.io;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class ReadUsingNIO {
    
    public static void main(String[] args) {
        Path demoPath = Paths.get("D:\s11.csv");
        Charset charset = Charset.forName("ISO-8859-1");
        try {
            List<String> lines = Files.readAllLines(demoPath, charset);
            for (String line : lines) {
               System.out.println(line);
            }
         } catch (IOException e) {
            System.out.println(e);
         }
    }
}
