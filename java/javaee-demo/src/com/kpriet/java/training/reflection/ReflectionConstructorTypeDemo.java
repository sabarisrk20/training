package com.kpriet.java.training.reflection;

/*
Requirement:
    To explain the types of constructors with an example.
    
Entity:
    ReflectionConstructorTypeDemo
    
Function declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1. Declare a integer and boolean variable.
    2. Create a constructor method without any arguements.
        2.1 Print a message.
    3. Create a constructor method with arguements.
        3.1 Print a message.
    4. Create objects for the class.
    5. Print the value and result as default constructor.
    
Pseudo code:

public class ReflectionConstructorTypeDemo {
    
    int value;
    boolean result;
    public ReflectionConstructorTypeDemo() {
        System.out.println("This is no arguement constructor");
    }
    public ReflectionConstructorTypeDemo(String type) {
        System.out.println("This is " + type + "constructor");
    }
    public static void main(String[] args) {

        ReflectionConstructorTypeDemo constructorType1 = new ReflectionConstructorTypeDemo();
        @SuppressWarnings("unused")
        ReflectionConstructorTypeDemo constructorType2 = new ReflectionConstructorTypeDemo("Parameterized");
        System.out.println("Example of default constructor");
        System.out.println(constructorType1.value);
        System.out.println(constructorType1.result);
    }
}

*/

public class ReflectionConstructorTypeDemo {
    
    int value;
    boolean result;
    public ReflectionConstructorTypeDemo() {
        System.out.println("This is no arguement constructor");
    }
    public ReflectionConstructorTypeDemo(String type) {
        System.out.println("This is " + type + "constructor");
    }
    public static void main(String[] args) {

        ReflectionConstructorTypeDemo constructorType1 = new ReflectionConstructorTypeDemo();
        @SuppressWarnings("unused")
        ReflectionConstructorTypeDemo constructorType2 = new ReflectionConstructorTypeDemo("Parameterized");
        System.out.println("Example of default constructor");
        System.out.println(constructorType1.value);
        System.out.println(constructorType1.result);
    }
}
