package com.kpriet.java.training.collections;

import java.util.Set;
import java.util.TreeSet;

/**
 * Problem Statement
 * 1.Java program to demonstrate insertions and string buffer in tree set
 * 
 * Requirement
 * 1.Java program to demonstrate insertions and string buffer in tree set
 * 
 * Entity
 * 1.TreeSetExercise
 * 
 * Jobs to be Done
 * 1.Create a TreeSet
 * 2.Add values to TreeSet
 * 3.Print the Elements in TreeSet.
 * 
 * Pseudo Code
 * class TreeSetExercise {
 * 
 * 		public static void main(String[] args) {
 *      
 *      	Set<String> newSet = new TreeSet<String>();
 *      
 *      	newSet.add(new StringBuffer("Sabari").toString());
 *      	newSet.add(new StringBuffer("Sabari Ramkumar").toString());
 *      	newSet.add(new StringBuffer("Santheep).toString());
 *      	newSet.add(new StringBuffer("Hariharasudhan").toString());
 *      
 *      	newSet.stream().forEach(x -> System.out.println(x));
 *      }
 * }
 * 
 * 
 * 
 *
 */

public class TreeSetExercise {
	
	public static void main(String[] args) {
		
		Set<String> newSet = new TreeSet<String>();
		
		newSet.add(new StringBuffer("Sabari").toString());
		newSet.add(new StringBuffer("Sabari Ramkumar").toString());
		newSet.add(new StringBuffer("Santheep").toString());
		newSet.add(new StringBuffer("Hariharasudhan").toString());
		
		newSet.stream().forEach(x -> System.out.println(x));
		
	}

}
