package com.kpriet.java.training.io.Stream;

/*
Requirement: 
    write a Java program reads data from a particular file using FileReader
    and write it to another, using FileWriter
    
Entity:
    FileReaderAndWriterDemo
     
Function Declaration:
    public static void main(String[] args);
  
Jobs to be done:
     1. Create a FileWriter and store it as null initially in variable fileWriter 
        1.1) then pass the destination file path as parameter to FileReader Class 
     2. Create a FileReader  and store it as null initially in variable fileReader 
        2.2) then pass the destination file path as parameter to Writer Class 
     3. While the file has not reached end.
        3.1) Read each characters and write it to destination file.
     4. Close the fleReader.
     5. Close the fileWriter.
     
PseudoCode:

public class FileReaderAndWriterDemo {
    
    public static void main(String[] args) throws IOException {  
        int value; 
        FileWriter fileWriter = null;
        FileReader fileReader = null; 
        fileReader = new FileReader("D:\\text.txt"); 
        fileWriter = new FileWriter("D:\\text.txt"); 
        while ((value = fileReader.read()) != -1)  {
            fileWriter.write(value); 
        }
        fileReader.close(); 
        fileWriter.close();
    } 
}

*/

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileReaderAndWriterDemo {
    
    public static void main(String[] args) throws IOException {  
        int value; 
        FileWriter fileWriter = null;
        FileReader fileReader = null; 
        fileReader = new FileReader("D:\\text.txt"); 
        fileWriter = new FileWriter("D:\\text.txt"); 
        while ((value = fileReader.read()) != -1)  {
            fileWriter.write(value); 
        }
        fileReader.close(); 
        fileWriter.close();
    } 
}
