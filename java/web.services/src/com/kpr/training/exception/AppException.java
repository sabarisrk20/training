package com.kpr.training.exception;

/**
 * Problem Statement
 * 1.Create a AppException
 * 
 * Entity
 * 1.AppException
 * 
 * Method Signature
 * 1.public AppException();
 * 2.public AppException(ErrorCodes code);
 * 3.public AppException(ErrorCodes code, Exception e);
 * 
 * Jobs to be Done
 * 1.Create a empty parameter constructor
 * 2.Create a Constructor with parameter of ErrorCodes as code
 * 		2.1)return the super errorCode and code messgae
 * 3.Create another constructor with ErrorCode and Exception as parameter
 * 		3.1)return the super with errorCode and error message and StackTrace 
 * 
 * 
 * 
 * Pseudo Code
 * 
 * class AppException extends RuntimeException{
 *	
 *	public AppException() {
 *		super();
 *	}
 *	
 *	public AppException(ErrorCodes code) {
 *		super("Error:" + code + " " + code.getMessage());
 *	}
 *	
 *	public AppException(ErrorCodes code, Exception e) {
 *		super(code + code.getMessage(), e);
 *	}
 *}
 */		

@SuppressWarnings("serial")
public class AppException extends RuntimeException{
	
	public AppException() {
		super();
	}
	
	public AppException(ErrorCodes code) {
		super("Error:" + code + " " + code.getMessage());
	}
	
	public AppException(ErrorCodes code, Exception e) {
		super(code + " " + code.getMessage(), e);
	}
}
