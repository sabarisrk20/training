package com.kpriet.java.training.dateTime;

/*
Requirement:
    To display the default time zone and Any three particular time zone.
     
Entity:
    DefaultTimeZoneDemo
    
Function Declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1. Set the default time zone to GMT.
    2. Create an instance for SimpleDateFormat as date Format and store the default date format.
    3. Create an instance for Date as date
    4. Format the date as dateFormat and store it in the String variable currentDateTime
    5. Print the currentDateTime.
    6. Get the different time zones and print that time zone.
    
Pseudo code:

class TimeZoneDemo {
    
    public static void main(String[] args) {
        TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy EEEE hh:mm:ss a");
        Date date = new Date();
        String currentDateTime = dateFormat.format(date);
        System.out.println("GMT-" + currentDateTime);
        
        TimeZone timeZone = TimeZone.getTimeZone("Europe/Copenhagen");
        dateFormat.setTimeZone(timeZone);
        currentDateTime = dateFormat.format(date);
        System.out.println("Europe/Copenhagen-" + currentDateTime);
        
        timeZone.getTimeZone("America/Managua");
        dateFormat.setTimeZone(timeZone);
        currentDateTime = dateFormat.format(date);
        System.out.println("America/Managua-" + currentDateTime);
        
        timeZone.getTimeZone("Pacific/Auckland");
        dateFormat.setTimeZone(timeZone);
        currentDateTime = dateFormat.format(date);
        System.out.println("Pacific/Auckland-" + currentDateTime);
        
        
    }
}
 */
import java.util.Date;
import java.util.TimeZone;
import java.text.SimpleDateFormat;

public class DefaultTimeZoneDemo {

	public static void differentTZTimings() {
		TimeZone timeZone = TimeZone.getTimeZone("GMT");
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy EEEE hh:mm:ss a");
		dateFormat.setTimeZone(timeZone);
		Date date = new Date();
		String currentDateTime = dateFormat.format(date);
		System.out.println("GMT-" + currentDateTime);

		timeZone = TimeZone.getTimeZone("Asia/Kolkata");
		dateFormat.setTimeZone(timeZone);
		currentDateTime = dateFormat.format(date);
		System.out.println("Asia/Kolkata-" + currentDateTime);

		timeZone = TimeZone.getTimeZone("Australia/Perth");
		dateFormat.setTimeZone(timeZone);
		currentDateTime = dateFormat.format(date);
		System.out.println("Australia/Perth-" + currentDateTime);

		timeZone = TimeZone.getTimeZone("Europe/Copenhagen");
		dateFormat.setTimeZone(timeZone);
		currentDateTime = dateFormat.format(date);
		System.out.println("Europe/Copenhagen-" + currentDateTime);

	}

	public static void main(String[] args) {
		System.out.println("Displaying current of the particular TimeZones");
		differentTZTimings();
	}

}
