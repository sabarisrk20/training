SELECT college.college_code
      ,college.college_name
      ,college.city
      ,college.state
      ,college.year_opened
      ,department.dept_name
      ,employee.emp_name
  FROM college college
  LEFT JOIN university university
    ON college.univ_code = university.univ_code
 RIGHT JOIN department department
    ON university.univ_code = department.univ_code
  LEFT JOIN employee employee 
    ON college.college_id = employee.college_id
  LEFT JOIN designation designation
    ON designation.desig_id = employee.desig_id
 WHERE designation.desig_name = "HOD"
   HAVING department.dept_name IN ('CSE','IT');