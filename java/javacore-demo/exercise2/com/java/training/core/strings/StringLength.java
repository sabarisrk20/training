/*
 * + How long is the string returned by the following expression? What is the
 * string? "Was it a car or a cat I saw?".substring(9, 12)
 * 
 * Requirement: To find how long is the string returned by the following
 * expression? What is the string? "Was it a car or a cat I saw?".substring(9,
 * 12)
 * 
 * Entities: No class is used in this program
 * 
 * Function Declaration: No function is declared in this program.
 * 
 * Solution: It's 3 characters in length: car. It does not include the space
 * after car.
 * 
 */