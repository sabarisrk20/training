CREATE TABLE `semester_fee` (
  `cdept_id` int DEFAULT NULL,
  `stud_id` int NOT NULL,
  `semester` tinyint NOT NULL,
  `amount` varchar(45) DEFAULT NULL,
  `paid_year` year DEFAULT NULL,
  `paid_status` varchar(10) NOT NULL DEFAULT 'Unpaid',
  PRIMARY KEY (`stud_id`),
  KEY `fk_cdept_id_sem_idx` (`cdept_id`),
  KEY `fk_student_id_sem_idx` (`stud_id`),
  CONSTRAINT `fk_student_id_sem` FOREIGN KEY (`stud_id`) REFERENCES `student` (`stu_id`)
);

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`,
                           `paid_year`,
                           `paid_status`)
VALUES (1
       ,1
       ,8
       ,'10000'
       ,2020
       ,'PAID');
       
INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`,
                           `paid_year`,
                           `paid_status`)
VALUES (1
       ,2
       ,8
       ,'10000'
       ,NULL
       ,'UNPAID');
       
INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`,
                           `paid_year`,
                           `paid_status`)
VALUES (2
       ,3
       ,8
       ,'10000'
       ,NULL
       ,'UNPAID');
       
INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`,
                           `paid_year`,
                           `paid_status`)
VALUES (2
       ,4
       ,8
       ,'10000'
       ,NULL
       ,'UNPAID');
       
INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`,
                           `paid_year`,
                           `paid_status`)
VALUES (2
       ,5
       ,6
       ,'10000'
       ,2020
       ,'PAID');
       
INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`,
                           `paid_year`,
                           `paid_status`)
VALUES (3
       ,6
       ,6
       ,'10000'
       ,2020
       ,'PAID');
       
INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`,
                           `paid_year`,
                           `paid_status`)
 VALUES (4
        ,7
        ,4
        ,'10000'
        ,NULL
        ,'UNPAID');
        
INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`,
                           `paid_year`,
                           `paid_status`)
 VALUES (4
        ,8
        ,4
        ,'10000'
        ,2020
        ,'PAID');
        
INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`,
                           `paid_year`,
                           `paid_status`)
 VALUES (5
        ,9
        ,4
        ,'10000'
        ,NULL
        ,'UNPAID');
        
INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`,
                           `paid_year`,
                           `paid_status`)
 VALUES (5
        ,10
        ,6
        ,'10000'
        ,2019
        ,'PAID');
        
INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`,
                           `paid_year`,
                           `paid_status`)
 VALUES (6
        ,11
        ,8
        ,'10000'
        ,NULL
        ,'UNPAID');
        
INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`,
                           `paid_year`,
                           `paid_status`)
 VALUES (6
        ,12
        ,2
        ,'10000'
        ,NULL
        ,'UNPAID');
        
INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`,
                           `paid_year`,
                           `paid_status`)
 VALUES (2
        ,13
        ,4
        ,'10000'
        ,2019
        ,'PAID');
        
INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`,
                           `paid_year`,
                           `paid_status`)
 VALUES (3
        ,14
        ,6
        ,'10000'
        ,2020
        ,'PAID');
        
INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`,
                           `paid_year`,
                           `paid_status`)
 VALUES (4
        ,15
        ,2
        ,'10000'
        ,NULL
        ,'UNPAID');
        
INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`,
                           `paid_year`,
                           `paid_status`)
VALUES (5
       ,16
       ,6
       ,'10000'
       ,2020
       ,'PAID');
       
INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`,
                           `paid_year`,
                           `paid_status`)
VALUES (6
        ,17
        ,4
        ,'10000'
        ,NULL
        ,'UNPAID');
        
INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`,
                           `paid_year`,
                           `paid_status`)
VALUES (1
        ,18
        ,2
        ,'10000'
        ,2018
        ,'PAID');
        
INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`,
                           `paid_year`,
                           `paid_status`)
VALUES (2
       ,19
       ,2
       ,'10000'
       ,NULL
       ,'UNPAID');
       
INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`,
                           `paid_year`,
                           `paid_status`)
VALUES (3
       ,20
       ,6
       ,'10000'
       ,NULL
       ,'UNPAID');
