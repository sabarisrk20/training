package com.kpr.training.exception;

/* 
 Create ErrorCode with Enum

Entity
 * ErrorCodes
 
Method Signature
 * ErrorCodes(String message);
 * public String getMessage();
  
Jobs to be Done
 * Create variable called code of type int
 * Create a variable called message of type String
 * Create a Constructor with code and message as parameter
 * Create another method called getCode of return type int that returns code
 * Create another method called getMessage of return type String that returns message
 * Create Enum type ErrorCodes with Code and Message
 
Pseudo Code
public enum ErrorCodesvalues {
	conFailure("Connection Failure"),
	postalError("Postal Code should not be Empty"),
	addressError("Address Creation Failure"),
	fieldEmpty("The Id Field cannot be Empty"),
	personUpdationError("Updation Failure"),
	invalidEmail("Invalid Email"),
	personError("Person Creation Failure"),
	closeConnectionError("Close Connection  Failure"),
	notUniqueEmail("Email is not unique"),
	emptyValues("The values are empty"),
	invalidname("First name and Last Name Are Same"),
	valuesAreEmpty("First name or Last Name or Birth date is empty"),
	dateInvalidFormate("Invalid Date Formate"),
	nameNotUnique("Person is Duplicate"),
	emptyAddress("Values are Empty");
	public String message;
	ErrorCodesvalues(String message) {
		this.message = message;
	}
	public String getMessage() {
		return this.message;
	}

}
 */

public enum ErrorCodesvalues {
	
	ERROR01("Connection Failure"),
	ERROR02("Postal Code should not be Empty"),
	ERROR03("Address Creation Failure"),
	ERROR04("The Id Field cannot be Empty"),
	ERROR05("Updation Failure"),
	ERROR06("Invalid Email"),
	ERROR07("Person Creation Failure"),
	ERROR08("Close Connection  Failure"),
	ERROR09("Email is not unique"),
	ERROR10("The values are empty for address"),
	ERROR11("First name and Last Name Are Same"),
	ERROR12("First name or Last Name is empty"),
	ERROR13("Invalid Date Formate"),
	ERROR14("Person is Duplicate"),
	ERROR15("Values are Empty for person"),
    ERROR16("Failed to read address"),
    ERROR17("Failed to read all addresses"),
	ERROR18("Failed to find unique address"),
	ERROR19("Failed to Delect address"),
	ERROR20("Failed to search"),
	ERROR21("Failed to update Person"),
    ERROR22("Failed to read person"),
    ERROR23("Failed to read all person"),
	ERROR24("Failed to Delect person");
	public String message;
	
	ErrorCodesvalues(String message) {
		this.message = message;
	}
	
	public String getMessage() {
		return this.message;
	}
}
