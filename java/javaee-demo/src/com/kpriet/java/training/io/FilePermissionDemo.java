/*
Requirement :
    Get the permission allowed for a file

Entity:
    FilePermissionDemo 

Function Declaration :
    public static void main(String[] args)

Jobs To Be Done :
    1.Declare the path in a type string and store it in variable path
    2.Create FilePermission class and pass the path as parameter
    3.Create PermissionCollection class and using the object of this class (permission)
     3.1) using permission object invoke add method and pass the parameter as object of FileParameter
     3.2) Check if it has the permission to read the file 
          if condition satisfied Print "Read permission is granted for the path"
     3.2) if the condition not satisfied
          Print "No Read permission is granted for the path " 
 */

package com.kpriet.java.training.io;

import java.io.FilePermission;
import java.security.PermissionCollection;

public class FilePermissionDemo {
    
    public static void main(String[] args) {
        String path = "D:\\java answer\\collection\\Iterator.java";
        FilePermission file = new FilePermission("D:\\java answer\\collection\\Iterator.java\\-","read");
        PermissionCollection permission = file.newPermissionCollection();
        permission.add(file);
        if (permission.implies(new FilePermission(path,"read"))) {
            System.out.println("Read permission is granted for the path "+path);
        } else {
            System.out.println("No Read permission is granted for the path "+ path); 
        }
    }
}
