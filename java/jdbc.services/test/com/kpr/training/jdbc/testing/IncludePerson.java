package com.kpr.training.jdbc.testing;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.model.Person;
import com.kpr.training.jdbc.services.PersonServices;
import com.kpr.training.jdbc.validator.PersonValidator;

public class IncludePerson {
	public void includePerson() throws Exception {
		PersonServices personService = new PersonServices();
		try (
	            Reader reader = Files.newBufferedReader(Paths.get("C:/Users/Sunil kumar/eclipse-workspace/jdbc.services/resorces/personinputs.csv"));
		        CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT
		                    .withFirstRecordAsHeader()
		                    .withIgnoreHeaderCase()
		                    .withTrim());
	        ) {
			
	            for (CSVRecord csvRecord : csvParser) {
	                String firstname = csvRecord.get("firstname");
	                String lastname = csvRecord.get("lastname");
	                String email = csvRecord.get("email");
	                String dob = csvRecord.get("birth_date");
	                String street = csvRecord.get("street");
	                String city = csvRecord.get("city");
	                String postal_code = csvRecord.get("postal_code");
	                int code = Integer.parseInt(postal_code);
	   				Date birthdate = PersonValidator.dateFormate(dob);
		            Address address = new Address(street,city,code);
		            Person person = new Person(firstname,lastname,email,birthdate,address);
		    	    personService.create(person);
	            }
	        }
	}
}
