package com.kpr.training.jdbc.constant;

public class QueryStatement {
	
	public static final String INSERT_ADDRESS_QUERY    = new StringBuilder("INSERT INTO address (street,city,postal_code)")
			                                                       .append("  VALUES (?,?,?)                             ").toString();
	public static final String UPDATE_ADDRESS_QUERY    = new StringBuilder("UPDATE address SET street = (?)      ")
													               .append("                  ,city = (?)        ")
													               .append("                  ,postal_code = (?) ")
														           .append("            WHERE  id = (?)          ").toString();
	public static final String READ_ALL_ADDRESS_QUERY  = new StringBuilder("SELECT  city       ")
			 											           .append("      ,id          ")		
														           .append("      ,street      ")			                    
			                                                       .append("      ,postal_code ")
														           .append(" FROM  address     ").toString(); 
	public static final String READ_ADDRESS_QUERY      = new StringBuilder("SELECT id          ")
														           .append("      ,city        ")	
			                                                       .append("      ,street      ")
														           .append("      ,postal_code ")
			                                                       .append(" FROM  address     ")
														           .append(" WHERE id = ?      ").toString();
	public static final String DELETE_ADDRESS_QUERY    = new StringBuilder("DELETE FROM address")
														           .append(" WHERE id=?        ").toString();
	public static final String UNIQUE_EMAIL_QUERY      = new StringBuilder("SELECT COUNT(*) AS count ")
												                   .append("  FROM person            ")
												                   .append(" WHERE email = ?         ").toString();
	public static final String INSERT_PERSON_QUERY     = new StringBuilder("INSERT INTO person(firstname,lastname,email,birth_date,address_id)")
														           .append("  VALUE (?,?,?,?,?)                                               ").toString();
	public static final String UPDATE_PERSON_QUERY     = new StringBuilder("UPDATE person SET firstname=?  ")
			 											           .append("                 ,lastname=?   ")
														           .append("                 ,email=?      ")
														           .append("                 ,birth_date=? ")
														           .append("                 ,address_id=?  ")
														           .append("            WHERE id=?         ").toString();
	public static final String UNIQUE_ADDRESS_QUERY    = new StringBuilder("SELECT id             ")
														   		   .append("  FROM address        ")
														   		   .append(" WHERE street =?      ")
														   		   .append("   AND city =?        ")
														   		   .append("   AND postal_code =? ").toString();
	public static final String READ_ALL_PERSON_QUERY   = new StringBuilder("SELECT p.firstname    ")
														  	       .append("      ,p.lastname     ")
														  	       .append("      ,p.email        ")
														  	       .append("  	  ,a.id           ")
														  	       .append("	  ,a.street       ")
														  	       .append("	  ,a.city         ")
														  	       .append("	  ,a.postal_code  ")
														  	       .append("      ,p.birth_date   ")
														  	       .append("      ,p.created_date ")
														  	       .append("  FROM person        p")
														  	       .append("      ,address a	  ")
														  	       .append(" Where a.id = p.address_id  ").toString(); 
	public static final String READ_PERSON_QUERY       = new StringBuilder("SELECT p.firstname			")
			 											           .append("      ,p.lastname			")
			 											           .append("      ,p.email				")
			 											           .append("	  ,p.address_id			")
			 											           .append("	  ,a.id					")
			 											           .append("	  ,a.street				")
			 											           .append("	  ,a.city				")
			 											           .append("	  ,a.postal_code		")
			 											           .append("	  ,p.birth_date			")
			 											           .append("	  ,p.created_date 	    ")
			 											           .append("  FROM person p			    ")
			 											           .append("	  ,address a 		    ")
			 											           .append(" Where a.id = p.address_id  ")
			 											           .append("   AND p.id=?               ").toString() ; 
	public static final String DELETE_PERSON_QUERY     = new StringBuilder("DELETE FROM person ")
			                                                       .append("  WHERE id=?       ").toString();
	public static final String UNUSED_ADDRESS_QUERY    = new StringBuilder("SELECT id 												")
			                                                       .append("  FROM address											")
			                                                       .append("  WHERE NOT EXISTS 										")
			                                                       .append("                 (SELECT 1 								")
			                                                       .append("                    FROM person 						")
			                                                       .append("                   WHERE address.id = person.address_id)").toString(); 
	public static final String CHECK_ADDRESS_QUERY     = new StringBuilder("SELECT id               ")
			                                                       .append("  FROM address          ")
			                                                       .append(" WHERE street = (?)     ")
			                                                       .append("   AND city= (?)        ")
			                                                       .append("   AND postal_code = (?)").toString();
	public static final String UNIQUE_NAME_QUERY       = new StringBuilder("SELECT COUNT(*) AS count    ")
	                                                               .append("  FROM person               ")
	                                                               .append("WHERE  firstname = ?        ")
	                                                               .append("  AND  lastname = ?         ").toString();
	public static final String SEARCH_ADDRESS_QUERY    = new StringBuilder("SELECT id			")
                                                                   .append("      ,street		")
                                                                   .append("      ,city			")
                                                                   .append("      ,postal_code  ")
                                                                   .append("  FROM address 		")
                                                                   .append(" WHERE (street)		")
                                                                   .append("  LIKE (?) 			")
                                                                   .append("    OR (city)		")
                                                                   .append("  LIKE (?) 			")
                                                                   .append("    OR (postal_code)")
                                                                   .append("  LIKE (?)          ").toString();
	
	}