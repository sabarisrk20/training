package com.kpriet.java.training.collections;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

/**
 * Problem Statement
 * 1.Demonstrate java program to working of map interface using( put(), remove(), booleanContainsValue(), replace() ).
 *   If possible try remaining methods.
 *   
 * Requirement
 * 1.Demonstrate java program to working of map interface using( put(), remove(), booleanContainsValue(), replace() ).
 *   If possible try remaining methods.
 *   
 * Entity
 * 1.MapInterface
 * 
 * Method Signature
 * 1.public void adder(Integer key,String value);
 * 2.public void remove(Integer value);
 * 3.public boolean searcher(Integer val);
 * 4.public void replacer(Integer> key, String val);
 * 
 * Jobs to be Done.
 * 1.Create a Map.
 * 2.Get the Value to add from the User.
 * 		2.1)Add Values to Map with adder method.
 * 3.Get the Value to remove from the User.
 * 		3.1)Remove values with remove method.
 * 4.Get the Value to Search from the User.
 * 		4.1)call searcher method to search value.
 * 5.Get the value to replace from the User.
 * 		5.1)Call replacer method to replace values in Map
 * 
 * Pseudo Code
 * public class MapInterface {
 *	
 *	public static Map<Integer,String> newMap = new HashMap<Integer,String>();
 *	public static Scanner scanner = new Scanner(System.in);
 *	
 *	public void adder(int size) {
 *		System.out.println("Enter "+size+" to Add");
 *		for(int i=0; i<=size-1; i++) {
 *			String valueIn = scanner.nextLine();
 *			newMap.put(i,valueIn);
 *		}
 *	}
 *	
 *	public void remove(Integer key) {
 *		newMap.remove(key);
 *		System.out.println("After Removing");
 *		for(Map.Entry<Integer, String> ite : newMap.entrySet()) {
 *			System.out.println("Key: " + ite.getKey() +" "+ "Value: "+ite.getValue());
 *		}
 *	}
 *	
 *	public boolean searcher(String val) {
 *		if(newMap.containsValue(val)) {
 *			return true;
 *		} else {
 *			return false;
 *		}
 *	}
 *	
 *	public void replacer(Integer key, String val) {
 *		newMap.replace(key, val);
 *	}
 *	
 *	public void printer() {
 *		for(Map.Entry<Integer, String> ite : newMap.entrySet()) {
 *			System.out.println("Key: " + ite.getKey() +" "+ "Value: "+ite.getValue());
 *		}
 *		
 *	}
 *	
 * 	public static void main(String [] args) {
 *		
 *		MapInterface map = new MapInterface();
 *		
 *		
 *		System.out.println("Enter the Size of the Map");
 * 		int size = scanner.nextInt();
 *		map.adder(size);
 *		map.printer();
 *		
 *		System.out.println("Enter the Value to Remove");
 *	 	int rem = scanner.nextInt();
 *		map.remove(rem);
 *		
 *		System.out.println("Enter the Value to Search");
 *		String ser = scanner.nextLine();
 *		if(map.searcher(ser)) {
 *			System.out.println(ser+"Value Exist");
 *		} else {
 *			System.out.println(ser+"Value does not Exist");
 *		}
 *		
 *		System.out.println("Enter the Key to Replace");
 *		Integer k = scanner.nextInt();
 *		System.out.println("Enter the New value to Replace");
 *		String val = scanner.nextLine();
 *	 	map.replacer(k, val);
 *		
 *		map.printer();
 *		
 *	}
 *
 *}
 * 
 * 
 */

public class MapInterface {
	
	public static Map<Integer,String> newMap = new HashMap<Integer,String>();
	public static Scanner scanner = new Scanner(System.in);
	
	public void adder(int size) {
		System.out.println("Enter "+size+" to Add");
		for(int i=0; i<=size-1; i++) {
			String valueIn = scanner.nextLine();
			newMap.put(i,valueIn);
		}
	}
	
	public void remove(Integer key) {
		newMap.remove(key);
		System.out.println("After Removing");
		for(Map.Entry<Integer, String> ite : newMap.entrySet()) {
			System.out.println("Key: " + ite.getKey() +" "+ "Value: "+ite.getValue());
		}
	}
	
	public boolean searcher(String val) {
		if(newMap.containsValue(val)) {
			return true;
		} else {
			return false;
		}
	}
	
	public void replacer(Integer key, String val) {
		newMap.replace(key, val);
	}
	
	public void printer() {
		for(Map.Entry<Integer, String> ite : newMap.entrySet()) {
			System.out.println("Key: " + ite.getKey() +" "+ "Value: "+ite.getValue());
		}
		
	}
	
	public static void main(String [] args) {
		
		MapInterface map = new MapInterface();
		
		
		System.out.println("Enter the Size of the Map");
		int size = scanner.nextInt();
		map.adder(size);
		map.printer();
		
		System.out.println("Enter the Value to Remove");
		int rem = scanner.nextInt();
		map.remove(rem);
		
		System.out.println("Enter the Value to Search");
		String ser = scanner.nextLine();
		if(map.searcher(ser)) {
			System.out.println(ser+"Value Exist");
		} else {
			System.out.println(ser+"Value does not Exist");
		}
		
		System.out.println("Enter the Key to Replace");
		Integer k = scanner.nextInt();
		System.out.println("Enter the New value to Replace");
		String val = scanner.nextLine();
		map.replacer(k, val);
		
		map.printer();
		
	}

}
