ALTER TABLE `student` 
ADD CONSTRAINT `staff_id_staff`
  FOREIGN KEY (`staff_id`)
  REFERENCES `staff`(`id`);
ALTER TABLE `staff` 
  CHANGE COLUMN `name` `name` VARCHAR(45) NOT NULL
 ,CHANGE COLUMN `subject` `subject` VARCHAR(45) NULL DEFAULT 'oops'
 ,ADD CHECK (age>=25)
 ,ADD PRIMARY KEY (`staff_id`)
 ,ADD INDEX `name` (`name` ASC) VISIBLE;