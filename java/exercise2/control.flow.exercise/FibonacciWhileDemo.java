package in.kpriet.training.java.module;

public class FibonacciWhileDemo {
    public void fiboWhile(int num3) {
        int sum, iteration = 1;
        int num1 = 0, num2 = 1;
        while(iteration <= num3) {
            System.out.print(num1 + " ");
            sum = num1 + num2;
            num1 = num2;
            num2 = sum;
            iteration++;
        }
    }
}