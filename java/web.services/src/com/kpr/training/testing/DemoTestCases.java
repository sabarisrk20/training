package com.kpr.training.testing;

import java.util.concurrent.CountDownLatch;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.kpr.training.jdbc.model.Address;
import com.kpr.training.service.AddressService;
import com.kpr.training.service.ConnectionService;

public class DemoTestCases {

	AddressService addressService = new AddressService();
	
	public Address address;
	public Address addressUpdate;
	public Address nullAddress;
	public CountDownLatch latch;
	long createResult;
	
	@BeforeMethod
	public void setUp() {

		address = new Address("35, Goundamani Nagar", "Madurai", 765876);

		addressUpdate = new Address("89, Santhanam Avenue", "Thanjavur", 987123);

		nullAddress = new Address("null", "null", 0);

		latch = new CountDownLatch(1);

	}
	@Test
	public void insertPerson() throws Exception {
		
		try {
			createResult = addressService.create(address);
			
			Assert.assertTrue(createResult > 0);
			
			if(createResult > 0) {
				ConnectionService.commitRollback(true);
			} else {
				ConnectionService.commitRollback(false);
			}
			ConnectionService.release();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
