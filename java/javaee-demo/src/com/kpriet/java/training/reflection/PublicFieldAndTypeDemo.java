package com.kpriet.java.training.reflection;

/*
Requirement:
    To write a program to obtain the all public fields and field type for the given code User.java.
  
Entity:
    PublicFieldAndTypeDemo
  
Method Signature:
    public static void main(String[] argv)
  
Jobs To Be Done:
    1) Assign a predefined class to the reference variable.
    2) Get the field names of that class and assign it to the variables.
    3) For each print the field name and its type.
  
Pseudo Code:

public class PublicFieldAndTypeDemo {
    
    public static void main(String[] args) throws Exception {
        Class<?> demo = java.lang.Thread.class;
        Field[] fields = demo.getFields();
        
        for (int intitalValue = 0; intitalValue < fields.length; intitalValue++) {
            System.out.println ("The Field is: " + fields[intitalValue].toString() + " and type is: "
                               + fields[intitalValue].getType());
        }
    }
}

*/
import java.lang.reflect.*;

public class PublicFieldAndTypeDemo {
    
    public static void main(String[] args) throws Exception {
        Class<?> demo = java.lang.Thread.class;
        Field[] fields = demo.getFields();
        
        for (int intitalValue = 0; intitalValue < fields.length; intitalValue++) {
            System.out.println ("The Field is: " + fields[intitalValue].toString() + " and type is: "
                               + fields[intitalValue].getType());
        }
    }
}
