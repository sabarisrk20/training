package com.kpr.training.servlet;

import java.io.BufferedReader;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.kpr.training.jdbc.model.Person;
import com.kpr.training.jsonutil.JsonUtil;
import com.kpr.training.service.PersonService;
import com.kpr.training.validator.Validator;

public class PersonServlet extends HttpServlet {
	
	//doPut -> update
	//doPost -> create
	//doDelete -> delete
	//doGet -> Read
	
	private static final long serialVersionUID = 1L;
	Person personClass = new Person();
	Person personGot;
	String personString;
	private static PersonService personService = new PersonService();
	private static JsonUtil jsonUtil = new JsonUtil();

	protected void doGet(HttpServletRequest req, HttpServletResponse res) {
		
		long personId = Long.parseLong(req.getParameter("id"));
		boolean includeAddress = Boolean.parseBoolean(req.getParameter("includeAddress"));
		
		try(PrintWriter writer = res.getWriter()) {
			
			if(personId!=0) {
				if(includeAddress) {
					personGot = personService.read(personId, true);
					personString = jsonUtil.jsonToString(personGot);
					writer.append(personString);
				} else {
					personGot = personService.read(personId, false);
					personString = jsonUtil.jsonToString(personGot);
					writer.append(personString);
				}
			} else {
				writer.append("Person Read Failure");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest req, HttpServletResponse res) {
		
		try {
			BufferedReader reader = req.getReader(); 
			PrintWriter writer = res.getWriter();
			StringBuilder personJson = new StringBuilder();
			
			String line = null;
			
			while((line = reader.readLine())!= null) {
				personJson.append(line);
			}
			
			//System.out.println(personJson);
			
			personGot = (Person) jsonUtil.stringToJson(personJson.toString(), personClass);
			
			System.out.println(personGot);
						
			long personId = personService.create(personGot);
			
			if(personId != 0) {
				writer.append("Person Creation Success : " + personId);
			} else {
				writer.append("Person Creation Failure");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	protected void doPut(HttpServletRequest req, HttpServletResponse res) {
		
		try {
			
			PrintWriter writer = res.getWriter();
			
			BufferedReader reader = req.getReader();
			
			StringBuilder personJson = new StringBuilder();
			
			String line = null;
			
			while((line = reader.readLine())!= null) {
				personJson.append(line);
			}
			
			Person person = (Person) jsonUtil.stringToJson(personJson.toString(), personClass);
			
			System.out.println(person);
			
			personService.update(person);
			
			res.setContentType("text/html");
			res.setCharacterEncoding("UTF-8");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	protected void doDelete(HttpServletRequest req, HttpServletResponse res) {
		
		try {
			
			BufferedReader reader = req.getReader();
			
			StringBuilder personId = new StringBuilder();
			
			String line = null;
			
			while((line = reader.readLine()) != null) {
				personId.append(line);
			}
			
//			Person person = (Person) jsonUtil.stringToJson(personId.toString(), personClass);
//			System.out.println(person.getId());
			personService.delete(Long.parseLong(req.getParameter("id")));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
