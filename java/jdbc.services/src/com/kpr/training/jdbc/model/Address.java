
/* POJO Class for Address

Entity
 * Address

Method Signature
 * public Address(String street, String city, int postal_code);
 * public String getStreet();
 * public String getCity();
 * public int getPostalCode();

Jobs to be Done
 * Create a id field of long type
 * Create a street field of String type
 * Create a city field of String type
 * Create a postal_code of int type
 * Create a toString() method and return id,street,city,postal_code.
 * Generate Constructors using fields.
 * Create a public constructor take all the Address fields
 * Create a Street getter of String return type
 * Create a city getter of String return type
 * Create a postalCode getter of int type

Pseudo Code
public class Address {

	public  long id;

	@Override
	public String toString() {
		return "Address [id=" + id + ", street=" + street + ", city=" + city + ", postal_code=" + postal_code + "]";
	}
	public String street;
	public String city;
	public long postal_code;
	public Address() {
		this.id=id;
		this.street = street;
		this.city = city;
		this.postal_code = postal_code;
	}
	public Address(String street, String city, long postal_code) {
		this.street = street;
		this.city = city;
		this.postal_code = postal_code;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public long getPostalCode() {
		return postal_code;
	}
	public void setPostal_code(long postal_code) {
		this.postal_code = postal_code;
	}	
}
 */
package com.kpr.training.jdbc.model;

public class Address {

	public long id;
	public String street;
	public String city;
	public int postalCode;

	public Address() {
		super();
	}

	public Address(String street, String city, int postalCode) {
		this.street = street;
		this.city = city;
		this.postalCode = postalCode;
	}
	
	public Address(long id,String street, String city, int postalCode) {
		this.id = id;
		this.street = street;
		this.city = city;
		this.postalCode = postalCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getPostalCode() {
		return postalCode;
	}

	public void setPostal_code(int postalCode) {
		this.postalCode = postalCode;
	}

    @Override
    public String toString() {
        		return new StringBuilder("Address = [id=")
							.append(id)
							.append(", street=")
							.append(street)
							.append(", city=")
							.append(city)			                    
							.append(", postalCode=")
							.append(postalCode).toString(); 
        		}
}
