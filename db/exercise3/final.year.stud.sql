SELECT student.roll_no
	  ,student.stu_name
      ,student.gender
      ,student.stu_dob
      ,student.stu_email
      ,student.stu_phone
      ,student.stu_address
      ,student.academic_year AS 'Batch'
      ,college.college_name
      ,department.dept_name
  FROM university
  LEFT JOIN college college
	ON university.univ_code = college.univ_code
 LEFT JOIN student student
	ON student.college_id = college.college_id
 LEFT JOIN college_department college_department
    ON  student.cdept_id = college_department.cdept_id
RIGHT JOIN department department
   ON college_department.udept_code = department.dept_code
 WHERE student.cdept_id = college_department.cdept_id
   AND university.university_name = 'Anna University'
   AND college.city = 'Coimbatore'
   AND student.academic_year = 2018