/*
    Problem Statement
    1.Print the type of the result value of following expressions
    2.what happens when mixing primitive with respective Wrapper types
    
    Enitity
    1.ExpressDemo Class
    
    Work Done
    1.Created a Wrapper Class type Integer a and Initialize the value 100.
    2.Created a Wrapper Class type Double b and c and Initialize the value as 10.5 and 12.4.
    3.Printing the Computation does not gives any error 
      since the Primitive type and the Wrapper types does not give error while Boxing the datatypes.
*/
/*
    Answers
    There will be no Big Difference and Error while performing arithmetic combining PrimitiveType and WrapperType.
*/

public class ExpressDemo {
    public static void main(String[] args) {
        Integer a = 100;
        Double b = 10.5;
        Double c = 12.4;
        Integer sm1 = a/24;
        Double sm2 = b/ 0.5;
        Double sm3 = c% 5.5;
        Integer sm4 = a % 56;
        System.out.println(sm1.getClass().getName() + sm1);
        System.out.println(100.10 / 10);
        System.out.println('Z' / 2);
        System.out.println(sm2.getClass().getName() + sm2);
        System.out.println(sm3.getClass().getName() + sm3);
        System.out.println(sm4.getClass().getName() + sm4);
        System.out.println('Z'*2);
    }
}