ALTER TABLE `student` ADD COLUMN `stud_id` VARCHAR(45) NOT NULL AFTER `roll_no`;
ALTER TABLE `staff` ADD `address` VARCHAR(25);
ALTER TABLE `staff` CHANGE COLUMN `address`  `mob_no` BIGINT NOT NULL;
ALTER TABLE `staff` MODIFY `mob_no` BIGINT;
ALTER TABLE `student` DROP `stud_id`;
