package com.kpriet.java.training.reflection;

/*
Requirement:
    To explain why does contructors does not returns values
    
Reason:
    Constructor does not return any value.

    1. While declaring a constructor you will not have anything like return type.
    2. In general, Constructor is implicitly called at the time of instantiation.
    3. It is not a method, its sole purpose is to initialize the instance variables.

*/