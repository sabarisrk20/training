package com.kpriet.java.training.collections;

import java.util.ArrayList;
import java.util.List;

/**
 * Problem Statement
 * 1. 8 districts are shown below
 *  Madurai, Coimbatore, Theni, Chennai, Karur, Salem, Erode, Trichy   to be converted to UPPERCASE.
 *  
 * Requirement
 * 1. 8 districts are shown below
 *  Madurai, Coimbatore, Theni, Chennai, Karur, Salem, Erode, Trichy   to be converted to UPPERCASE.
 *  
 * Method Signature
 * 1.public void caseConverter(List<String> list);
 * 2.public void printer(List<String> list);
 * Entity
 * 1.UpperCase
 * 
 * Jobs to be Done.
 * 1.Create a List of Districts.
 * 2.Convert the Distrits to UpperCase
 * 3.Print the list
 * 
 * Pseudo Code
 * 
 * class UpperCase {
 *	
 *	public void caseConverter(List<String> list) {
 *		list.replaceAll(String::toUpperCase);
 *	}
 *	
 *	public void printer(List<String> list) {
 *		list.stream().forEach(x -> System.out.println(x));
 *	}
 *	
 *	public static void main(String[] args) {
 *		
 *		UpperCase upperCase = new UpperCase();
 *		
 *		List<String> districtList = new ArrayList<String>();
 *		
 *		districtList.add("Tiruppur");
 *		districtList.add("Madurai");
 *		districtList.add("Theni");
 *		districtList.add("Chennai");
 *		districtList.add("Karur");
 *		districtList.add("Salem");
 *		districtList.add("Erode");
 *		districtList.add("Trichy");
 *		
 * 		
 *		upperCase.caseConverter(districtList);
 *		upperCase.printer(districtList); 
 *	}
 *
 *}
 *
 *
 */

public class UpperCase {
	
	public void caseConverter(List<String> list) {
		list.replaceAll(String::toUpperCase);
	}
	
	public void printer(List<String> list) {
		list.stream().forEach(x -> System.out.println(x));
	}
	
	public static void main(String[] args) {
		
		UpperCase upperCase = new UpperCase();
		
		List<String> districtList = new ArrayList<String>();
		
		districtList.add("Tiruppur");
		districtList.add("Madurai");
		districtList.add("Theni");
		districtList.add("Chennai");
		districtList.add("Karur");
		districtList.add("Salem");
		districtList.add("Erode");
		districtList.add("Trichy");
		
		
		upperCase.caseConverter(districtList);
		upperCase.printer(districtList);

		
		
	}

}
