CREATE TABLE `training_exp1`.`student` (
  `id` INT NULL,
  `name` VARCHAR(45) NULL,
  `dob` VARCHAR(45) NULL);
INSERT INTO `student` VALUES ('115', 'santheep', '2000-12-20');  
SELECT id
      ,name
      ,dob FROM student;
TRUNCATE TABLE student;
SELECT id
      ,name
      ,dob FROM student;