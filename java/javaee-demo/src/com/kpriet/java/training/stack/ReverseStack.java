package com.kpriet.java.training.stack;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.Vector;

/**
 * Problem Statement
 * 1.Reverse List Using Stack with minimum 7 elements in list
 * 
 * Requirement
 * 1.Reverse List Using Stack with minimum 7 elements in list
 * 
 * Method Signature
 * 1.private void insertBottom (Integer val);
 * 2.private void reverse();
 * 
 * Entity
 * 1.ReverseStack 
 * 
 * Jobs to be Done
 * 1.Create a Stack and add elements to the Stack
 * 2.reverse by doing the Following
 * 		2.1)Check if the Stack is empty or not , If push the value to the Stack
 * 		2.2)If the Stack is not Empty
 * 			2.2.1)Peek the Element to temp value 
 * 			2.2.2)Pop the Element out of Stack
 * 			2.2.3)Do the above steps repeateadly until the Stack becomes empty
 * 3.Then insert the element one by one to the Bottom of the Stack in the popped order 
 * 4.Print the reversed Stack. 
 * 
 * Pseudo Code
 * class ReverseStack {
 *	
 *	static Stack<Integer> newStack = new Stack<Integer>();
 *	
 *	private void insertBottom (Integer val) {
 *		if(newStack.isEmpty()) {
 *			newStack.push(val);
 *		} else {
 *			Integer temp = newStack.peek();
 *			newStack.pop();
 *			insertBottom(val);
 *			
 *			newStack.push(temp);
 *		}
 *	}
 *	
 *	private void reverse() {
 *		if(newStack.size() > 0) {
 *			Integer temp1 = newStack.peek();
 *			newStack.pop();
 *			reverse();
 *			
 *			
 *			insertBottom(temp1);
 *		}
 *	}
 *	
 *	public static void main(String[] args) {
 *		
 *		newStack.add(10);
 *		newStack.add(20);
 *		newStack.add(30);
 *		newStack.add(40);
 *		newStack.add(50);
 *		newStack.add(60);
 *		newStack.add(70);
 *		
 *		ReverseStack stack = new ReverseStack();
 *		
 *		System.out.println(newStack);
 *		
 *		stack.reverse();
 *		
 *		System.out.println(newStack);
 *	}
 *
 *}
 * 
 *
 */


public class ReverseStack {
	
	static Stack<Integer> newStack = new Stack<Integer>();
	
	private void insertBottom (Integer val) {
		if(newStack.isEmpty()) {
			newStack.push(val);
		} else {
			Integer temp = newStack.peek();
			newStack.pop();
			insertBottom(val);
			
			newStack.push(temp);
		}
	}
	
	private void reverse() {
		if(newStack.size() > 0) {
			Integer temp1 = newStack.peek();
			newStack.pop();
			reverse();
			
			
			insertBottom(temp1);
		}
	}
	
	public static void main(String[] args) {
		
		newStack.add(10);
		newStack.add(20);
		newStack.add(30);
		newStack.add(40);
		newStack.add(50);
		newStack.add(60);
		newStack.add(70);
		
		ReverseStack stack = new ReverseStack();
		
		System.out.println(newStack);
		
		stack.reverse();
		
		System.out.println(newStack);
	}

}
