Requirement
1.Implement the methods doPost() for Address servlet

Entity:
1.AddressServlet 

Method Signature
1.public void doPost(HttpServletRequest req, HttpServletResponse res) throws Exception {};

Jobs to be Done
1.Get address JSON from servlet request and store it in addressJSON 
2.Prepare jsonUtil to JsonUtil
2.Parse the addressJSON to Address Object called address.
3.Prepare the AddressService object called addressService.
4.invoke the create method from addressService and pass the person object and stroe the generated key 
  to addressId of long type
5.Parse the addressId to the HttpServletResponse body.

Pseudo Code

public class AddressServlet {
    
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
        String street = req.getParameter("street");
        String city = req.getParameter("city");
        long postalCode = req.getParameter("postalCode");
        
        AddressService addressService = new AddressService();
        
        long addressId = addressService.create(new Address(street, city, postalCode);
        
        res.sendResponse(addressId);
    }
    
}