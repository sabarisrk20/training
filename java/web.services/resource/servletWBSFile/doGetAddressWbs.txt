Requirement
1.To implement the doGet() for Address Servlet

Entity
1.AddressServlet
2.AddressService

Method Signature
1.public void doGet(HttpServletRequest req, HttpServletResponse res) throws Exception ;

Jobs to be Done
1.Get the address JSON from servlet request and store it in addressJSON
2.Prepare jsonUtil to JsonUtil
3.Parse the addressJSON to addressId of type int.
4.Prepare the AddressService object called addressService
5.invoke the read method from addressService and pass the addressId.
  and store the result in the address Object called addressGot.
6.Parse the addressGot object to the HttpServletResponse body.

Pseudo Code

public class AddressServlet {
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws Exception  {
        int addressId = req.getParameter("id");
        
        AddressService addressService = new AddressService();
        
        Address addressGot = addressService.read(addressId);
        
        res.setValue(addressGot);
    }
}

}