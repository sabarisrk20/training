SELECT s.roll_no
      ,s.stu_name
      ,c.college_name
      ,se.grade
      ,se.credits
      ,se.semester
      ,se.gpa
  FROM student s
      ,semester_result se
      ,college c
 WHERE s.stu_id=se.stud_id 
   AND c.college_id = s.college_id
ORDER BY c.college_name,se.semester;