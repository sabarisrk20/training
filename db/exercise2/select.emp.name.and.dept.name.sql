SELECT employee.first_name
    ,employee.surname
    ,department.name
  FROM employee employee
      ,department department
 WHERE employee.department_id = department.id;