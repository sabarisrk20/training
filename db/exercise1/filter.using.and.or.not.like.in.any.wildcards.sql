SELECT name AS Student_Name 
  FROM student 
 WHERE name='santheep' 
   AND age='19';

SELECT name AS Student_Name
  FROM student 
 WHERE age='17' 
    OR age='19';

SELECT name AS Student_Name
  FROM student 
 WHERE NOT age='19';
 
SELECT name AS Student_Name
  FROM student 
 WHERE name LIKE "s%";

SELECT name AS Student_Name
  FROM student 
 WHERE name IN ('hari','santheep');

SELECT name AS Student_Name
  FROM student
 WHERE staff_id = ANY (SELECT id FROM staff WHERE age > '33');
 
SELECT name AS Student_Name
  FROM student
 WHERE name LIKE "sa____" ;

SELECT name AS Student_Name
  FROM student
WHERE (age > '18');

