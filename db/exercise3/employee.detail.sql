SELECT employee.emp_name
      ,employee.dob
      ,employee.email
      ,employee.phone
      ,college.college_name
      ,college.city
      ,department.dept_code
      ,department.dept_name
      ,designation.desig_name
      ,designation.desig_rank
  FROM employee employee
  LEFT JOIN college college
    ON employee.college_id = college.college_id
  LEFT JOIN college_department
    ON employee.cdept_id = college_department.cdept_id
  LEFT JOIN department department
    ON college_department.udept_code = department.dept_code
  LEFT JOIN designation designation
    ON employee.desig_id = designation.desig_id
 ORDER BY designation.desig_rank
         ,college.college_name;