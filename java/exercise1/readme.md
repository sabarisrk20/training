Core Java - Basic Exercise
This directory contains the code that we used to practicing basic Core Java functionalities. Here you can see my log of works done.

Logs

1.Installed JRE and JDK ver 1.8
2.Environmental Variable assigned.
3.Succeeded in running Java Progrom on Local JRE.
4.Public Class/ Properties/ Methods.
5.Added inner class with package and also used extends for the CurrencyConverter.java
    1.Inner Class (Currency Class)
    2.Package created for the time Class
    3.Extends used for the Distance Class
6.Diagrams added for why object cannot be created for the abstract and interface class
7.Example for abstract and interface class finished (Space.java)