package com.kpriet.java.training.dateTime;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.temporal.TemporalAdjusters;
import java.util.Scanner;

/*
Requirement:
    To code for list of all the Saturdays in the current year of the given month.
    
Entity:
    ListOfMonday
    
Function Declaration:
    public static void main(String[] args) 
    
Jobs to be done:
    1.Get the month from user
    2.Create instance of  Month class and pass the user input as parameter
    3.Create instance the LocalDate class and pass the parameter and store it in its object 
    4.With that object get the monday's date and print it  
    5.Use the loop and check the month
      and print the next mondays's date
      
 Pseudo code:
 
 class ListOfSaturdays {
     
     public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the month to which list of Mondays to find: ");
        String givenMonth = scanner.nextLine();
        Month month = Month.valueOf(givenMonth.toUpperCase());
        LocalDate localDate = Year.now().atMonth(month).atDay(1)
                              .with(TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));
        
        Month newMonth = localDate.getMonth();
        while (newMonth == month) {
            System.out.println(localDate);
            localDate = localDate.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
            newMonth = localDate.getMonth();
        }
        
        scanner.close();
    }
}
*/

public class ListOfMondays {
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the month to which list of mondays to find:\n ");
        String givenMonth = scanner.nextLine();
        Month month = Month.valueOf(givenMonth.toUpperCase());
        System.out.printf("For the month of %s%n", month);
        LocalDate localDate = Year.now().atMonth(month).atDay(1)
                              .with(TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));
        
        Month newMonth = localDate.getMonth();
        while (newMonth == month) {
            System.out.println(localDate);
            localDate = localDate.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
            newMonth = localDate.getMonth();
        }
        
        scanner.close();
    }
}

