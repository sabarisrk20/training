package com.kpriet.java.training.user.exception;

import java.sql.SQLException;

/**
 * Problem Statement
 * 1.Explaining the Program
 * 
 * Requirement
 * 1.Explaining the Program
 * 
 * Method Signature
 * 
 * 
 * Entity
 * 1.UserException
 * 
 * Jobs to be Done
 * 1.Created a Class called UserException and created a try block with readPerson method of a object dao
 * 2.Then caught a SQLException that is been thrown by the readPerson method of the try block 
 * 3.The catch block catches SQLException and return a User Defined Exception and prints as "wrong" with 
 *   a SQLException variable..
 * 
 * 
 */

public class UserException {
	
	public static void main(String[] args) {
		/*
		try{
	        dao.readPerson();
	    } catch (SQLException sqlException) {
	        throw new MyException("wrong",sqlException);
	    }
	    */
	}

}