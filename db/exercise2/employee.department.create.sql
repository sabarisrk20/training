  CREATE TABLE `employee` (`id` INT NOT NULL
                          ,`first_name` VARCHAR(45) NOT NULL
                          ,`surname` VARCHAR(45) DEFAULT NULL
                          ,`dob` DATE NOT NULL
                          ,`date_of_joining` DATE NOT NULL
                          ,`annual_salary` VARCHAR(45) NOT NULL
                          ,`department_id` INT DEFAULT NULL);
  
  CREATE TABLE `department` (`id` INT NOT NULL PRIMARY KEY
                            ,`name` VARCHAR(45) DEFAULT NULL);