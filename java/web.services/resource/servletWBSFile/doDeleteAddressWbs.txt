Slide 1
Today my Topic is about Client Server Programming.

Slide 2
Here all the Client and the Server is a Process, processed by some porgramming languages.
Client is the program that needs a Data to process the instructions from the Server
Server is the program which has handles the requests from the client and sends the needed data to the Client

Slide 3
These Client and Server are just programms until it is sent over the Internet
So here comes the Application Programming Interface, This interface helps the Client and Server to 
get Connected over the Internet make them interact
The Common API amoung them are 
1.Socket Interface
2.Transport Later Interface
3.STREAM

Slide 4
So While Sending the data from the Physical System to another Physical System over Net 
There is a need of Internet and here
The Communication between client and server is done between two Sockets
Socket is the end to end point where we send and receive data with SocketAddress for both Client and
Server.
Here is the Example for the Client Server Model with Socket

Slide 5
So Sockets are nothing without SocketAddress
A SocketAddress is a combiniation of Internet Protocol and Port Number
The SocketAddress pair is essential for Communication between Server and Client
The Socket Address contains a Sender and a Receiver
IP Address is reperesented by 32bit and PortNumber is represented by 16-bit
As there in the Slide a IP Address can have nearly 65000 port numbers
the Port number in the SocketAddress is really important for data transfer
without the port number the client or server wont be able to send and receive date in internet

Slide 6
Lets see the SocketAddress structure in the Server Side

Slide 7
As we saw earlier we need a SocketAddress pair to establish communication with client and server
So The First We need is LocalSocketAddress
In Server Side the Local Socket is the Opearting System of the Server Program where it is placed.
The OS finds the Ip address of the System and makes use of it.
As in the Slide the when a HTTP request takes the port number of 8080, no other programs or HTTPS 
request takes the 8080 PORT, when the connection is established no other data can be sent and received 
through 8080 Port
The Port NUmber of a Program can be hardcoded by the developer or programmed to select automatically

Slide 8
The SocketAddress necessary for the Data transmission is Remote Socket Address
The Remote Socket Address is the Client program that need to be connected to the Server.
The Client SocketAddress contains the requests need to process the instruction of the client program
The Client SocketAddress is changed on every session.
but the SocketAddress of the Server never Changes..

Slide 9
Lets see the SocketAddress Structure in the Client Side

Slide 10
Also here the Client need both of the Local and Remote SocketAddress to establish the Communication

Here LOcal Socket represents the Client program running machine
Remote Socket represents the Server program 
Same as the Server the client also prepares the PORT number to the IP address and gets start to connect 
to the Server
The OS of the Client program makes sure the PORT is not consumed by any other processes.

Slide 11
When the Client needs to Connect to the Server
There occurs the Two Cases
1.When the User needs to connect to the Server for testing and program building process
2.While not knowing the IP Address of the Service or other end receiver Address

The First case happens only when the Client is made to connect for the Testing purposes
The Second case happens while the sender does not knows the receiver address to send data.
such as the the server or receiver are email id of a person, web pages, etc..

Everytime we cannot use the IP Address of the receiver or sender to communicate.
So here comes the DNS - Domain Name System. This connects the IP Address of a server to the Domain 
name such as www.facebook.com,and so on..
This fix the Second Case problem..

Slide 12
Next comes the Service of Transport Layer
Transport Layer provides the Pair of Process that makes the Users to connect to the internet
There are 3 types of Layers.
1.UDP
2.TCP
3.SCTP

Slide 13
UDP PROTOCOL
Udp protocol is unreliable and a datagram service
and it cannot be depended always 
this type of Prototype is used for light weight and speed message delivery systems
And UDP connection cannot assure the Heavy byte transmission im the internet.
Some Application uses the UDP are:
1.DNS - Domain Name Service
2.NNP - Networkk News Protocole

Slide 14
TCP protocol is another type of connection like UDP
TCP connection is reliable and ordered.
TCP connection carefully receives the Data Fully wiothout any corruption in the data
This works on 
1.Three way handshake,
2.Retransmission,
3.Error-Detection
This Connection makes sures the data is fully transmitted in correct order.
Some Application uses the TCP are:
1.Netflix
2.Whatsapp

Slide 15
The Next type of Connection is SCTP
This connection is a reliable and transmittes both ordered and unordered data into Streams
The Data is sent and revceived as the groups of bytes
This Connection places the messages into chunks a (piece of information) and each chunk is managed and 
moniotred by the chunk header
This protocol fragments a message into multiple chunks, each data chunk is sent to the each users for
data transfer.
Features of SCTP
1.Explicit partial Reliability
2.Improved Error detection 

--------------------END OF PRESENTATION--------------------


