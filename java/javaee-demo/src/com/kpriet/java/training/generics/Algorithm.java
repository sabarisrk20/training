package com.kpriet.java.training.generics;

/**
 * Problem Statement
 * 1.Will the following class compile? If not, why?
 *		public final class Algorithm {
 *   		public static <T> T max(T x, T y) {
 *       			return x > y ? x : y;
 *   		}
 *		}
 *
 * Requirement
 * 1.Will the following class compile? If not, why?
 *		public final class Algorithm {
 *   		public static <T> T max(T x, T y) {
 *       			return x > y ? x : y;
 *   		}
 *		}
 *
 * Entity
 * 1.Algorithm
 * 
 * Method Signature
 * 1.public static <T> T max(T x , T y);
 * 
 * Answer:
 * 1.No,The Following Class will not Compiler since the parameter and the return type is Generic Object. Since the Generic Object
 *   can't be predicted to compared using unary and binary operators.
 * 
 *
 */

public final class Algorithm {
	/*	
	public static <T> T max(T x, T y) {
        return x > y ? x : y;
    }
	*/
}
