/*+ Show two ways to concatenate the following two strings together to get the string "Hi, mom.":
    String hi = "Hi, ";
    String mom = "mom.";

Entities:
  No class is used in this program.

Function Declaration:
  No function is declared in this program.

By executing in Two Ways
      1.concat Function of String Class
      2.+ Concatenation Operator.

//Two Ways to Concatenate the Strings//
        String res = hii.concat(ram);//Using concat Function of String Class//
        System.out.println(res);
        String res1 = hii + ram; //Using + (String Concatenation) operator //
        System.out.println(res1);
    }
*/
package com.java.training.core.strings;

import java.util.*;

class StringConcatenate {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		String string1, string2;
		// Using concat Function of String Class//
		System.out.println("Enter the First String to concatinate as first String");
		string1 = scan.next(); // INPUT : hii
		System.out.println("Enter the second String to concatinate as second String");
		string2 = scan.next(); // INPUT : ram
		System.out.println(string1.concat(string2)); // OUTPUT : hiiram
		// Using + (String Concatenation) operator //OUTPUT : hiiram
		System.out.println(string1 + string2);
	}
}