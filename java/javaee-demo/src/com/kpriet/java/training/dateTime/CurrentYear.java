package com.kpriet.java.training.dateTime;
/*
Problem Statement:

5.Write a Java program to get the information of current/given year

---------------------------------WBS------------------------------------
1.Requirement :
    - Java program to get the information of current/given year
    
2.Entity:
    - CurrentYear

3.Method declaration:
    - public static void main(String[] args)
    
4.Jobs To be Done:
    1.Invoke Year class get the current year using now method and print it.
    2.Check the year leap year or not using isLeap method
    3.Print length of the year using length method and store it in length integer.
    4.Print the length of the year.
      
Pseudo code:
''''''''''''

public class CurrentYear {
	public static void main(String[] args) {
	 Year year = Year.now();
     System.out.println("Current Year: " + year);  
     if(year.isLeap()) {
    	 System.out.println("Current is leap year");
     } else {
    	 System.out.println("Current is not leap year");
     }
     int length = year.length();
     System.out.println("Length of the year: " + length+" days\n"); 
	}
}
---------------------------------Program Code------------------------------------
 */


import java.time.Year;

public class CurrentYear {

	public static void main(String[] args) {
	 Year year = Year.now();
     System.out.println("Current Year: " + year);  
     if(year.isLeap()) {
    	 System.out.println("Current is leap year");
     } else {
    	 System.out.println("Current is not leap year");
     }
     int length = year.length(); // 365
     System.out.println("Length of the year: " + length+" days"); 
	}
}
