abstract class Sun {
    public abstract void Printsun();
}

interface Galaxy {
    public void Milkyway();
    public void Andromeda();
}

class PrintAll extends Sun implements Galaxy {
    public void Printsun() {
        System.out.println("Some of the Stars i know: ");
        System.out.println("1). Sun.");
        System.out.println("2). VY Canis Majoris.");
        System.out.println("3). NML Cygni.");
        System.out.println("4). UY Scuti.");
    }
    
    public void Milkyway() {
        System.out.println("The Galaxy we live in is MilkyWay.");
    }
    
    public void Andromeda() {
        System.out.println("Our Neighbour Galaxy is Andromeda.");
    }
    
    public static void main(String args[]) {
        PrintAll pa = new PrintAll();
        pa.Printsun();
        pa.Milkyway();
        pa.Andromeda();
    }
}
/*
public class Space {
    public static void main(String args[]) {
        Sun sun = new PrintAll();
        PrintAll pa = new PrintAll();
        sun.Printsun();
        pa.Milkyway();
        pa.Andromeda();
    }
}*/