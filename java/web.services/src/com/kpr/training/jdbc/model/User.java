package com.kpr.training.jdbc.model;

public class User {
	
	private long id;
	private String user_role;
	private String userName;
	private String password;
	
	public User() {
		
	}

	public User(long id, String user_role, String userName, String password) {
		this.id = id;
		this.user_role = user_role;
		this.userName = userName;
		this.password = password;
	}
	
	public User(String role, String userName, String password) {
		this.user_role = role;
		this.userName = userName;
		this.password = password;
	}
	
	public User(long id, String userName, String password ) {
		this.id = id;
		this.userName = userName;
		this.password = password;
	}
	
	public User(String userName, String password) {
		this.userName = userName;
		this.password = password;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRole() {
		return user_role;
	}

	public void setRole(String role) {
		this.user_role = user_role;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return new StringBuilder("User [id=").append(id)
				.append(", role=").append(user_role)
				.append(", userName=").append(userName)
				.append(", password=").append(password).append("]")
				.toString();
	}

}
