package com.kpriet.java.training.dateTime;
/*
Problem Statement:

8. Write a Java program to get the maximum and minimum valueof the year, month, week, date 
from the current date of a default calendar.

1.Requirement :
    - Program to get the maximum and minimum valueof the year, month, week, date 
from the current date of a default calendar.
 
2.Entity:
    - MaxMinCalender

3.Method declaration:
    - public void maximumDate()
    - pubic void minimumDate()
    - public static void main(String[] args)
    
4.Jobs To be Done:
    1.Invoke MaxMinCalender class maximumDate and minimumDate method.
    2.Invoke Calender class getInstance method and store calender in it.
    3.Print all maximum values in maximumDate method.
    4.Print all minimum values in minimumDate method.

Pseudo code:
''''''''''''

public class MaxMinCalender {
	
	public void maximumDate() {
		Calendar calendar = Calendar.getInstance();
		System.out.println("\tCurrent Date and Time:" + calendar.getTime());
		int actualMaxYear = calendar.getActualMaximum(Calendar.YEAR);
		int actualMaxMonth = calendar.getActualMaximum(Calendar.MONTH);
		int actualMaxWeek = calendar.getActualMaximum(Calendar.WEEK_OF_YEAR);
		int actualMaxDate = calendar.getActualMaximum(Calendar.DATE);
		System.out.println("\tActual Maximum Year: " + actualMaxYear);
		System.out.println("\tActual Maximum Month: " + actualMaxMonth);
		System.out.println("\tActual Maximum Week of Year: " + actualMaxWeek);
		System.out.println("\tActual Maximum Date: " + actualMaxDate + "\n");
	}
	public void minimumDate() {
		Calendar calendar = Calendar.getInstance();
		System.out.println("\tCurrent Date and Time:" + calendar.getTime());		
		int actualMaxYear = calendar.getActualMinimum(Calendar.YEAR);
		int actualMaxMonth = calendar.getActualMinimum(Calendar.MONTH);
		int actualMaxWeek = calendar.getActualMinimum(Calendar.WEEK_OF_YEAR);
		int actualMaxDate = calendar.getActualMinimum(Calendar.DATE);
		System.out.println("\tActual Minimum Year: "+actualMaxYear);
		System.out.println("\tActual Minimum Month: "+actualMaxMonth);
		System.out.println("\tActual Minimum Week of Year: "+actualMaxWeek);
		System.out.println("\tActual Minimum Date: "+actualMaxDate+"\n");
	}
	public static void main(String[] args) {
		MaxMinCalender maxMinCalender = new MaxMinCalender();
		System.out.println("Maximum Value:-");
		maxMinCalender.maximumDate();
		System.out.println("Minimum Value:-");
		maxMinCalender.minimumDate();
	}
}
  
*/


import java.util.Calendar;

public class MaxMinCalender {
	
	public void maximumDate() {
		Calendar calendar = Calendar.getInstance();
		System.out.println("\tCurrent Date and Time:" + calendar.getTime());
		int actualMaxYear = calendar.getActualMaximum(Calendar.YEAR);
		int actualMaxMonth = calendar.getActualMaximum(Calendar.MONTH);
		int actualMaxWeek = calendar.getActualMaximum(Calendar.WEEK_OF_YEAR);
		int actualMaxDate = calendar.getActualMaximum(Calendar.DATE);

		System.out.println("\tActual Maximum Year: " + actualMaxYear);
		System.out.println("\tActual Maximum Month: " + actualMaxMonth);
		System.out.println("\tActual Maximum Week of Year: " + actualMaxWeek);
		System.out.println("\tActual Maximum Date: " + actualMaxDate + "\n");
	}
	public void minimumDate() {
		Calendar calendar = Calendar.getInstance();
		System.out.println("\tCurrent Date and Time:" + calendar.getTime());		
		int actualMaxYear = calendar.getActualMinimum(Calendar.YEAR);
		int actualMaxMonth = calendar.getActualMinimum(Calendar.MONTH);
		int actualMaxWeek = calendar.getActualMinimum(Calendar.WEEK_OF_YEAR);
		int actualMaxDate = calendar.getActualMinimum(Calendar.DATE);
		
		System.out.println("\tActual Minimum Year: "+actualMaxYear);
		System.out.println("\tActual Minimum Month: "+actualMaxMonth);
		System.out.println("\tActual Minimum Week of Year: "+actualMaxWeek);
		System.out.println("\tActual Minimum Date: "+actualMaxDate+"\n");

	}
	
	public static void main(String[] args) {
		MaxMinCalender maxMinCalender = new MaxMinCalender();
		System.out.println("Maximum Value:-");
		maxMinCalender.maximumDate();
		System.out.println("Minimum Value:-");
		maxMinCalender.minimumDate();
	}
}
