/*
 * + What is the initial capacity of the following string builder? StringBuilder
 * sb = new StringBuilder("Able was I ere I saw Elba."); Requirement: To find
 * the initial capacity of the following string builder StringBuilder sb = new
 * StringBuilder("Able was I ere I saw Elba.");
 * 
 * Entities: No class is used in this program
 * 
 * Function Declaration: No function is declared in this program
 * 
 * Solution: Initial capacity is found by adding 16 to the length of the initial
 * string length of the initial string=26 26 + 16 = 42.
 */