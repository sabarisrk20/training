package com.kpriet.java.training.collections;

import java.util.List;

/**
 * Problem Statement
 * 1.Check if the above person is in the roster list obtained from Person class.
 * 
 * Requirement
 * 1.Check if the above person is in the roster list obtained from Person class.
 * 
 * Method Signature
 * 
 * Entity
 * 1.PersonChecker
 * 
 * Jobs to be Done
 * 1.Invoke the Person Class createRoster method and store it in persons list.
 * 2.Filter the persons list 
 * 		2.1)If the person of name is equal to Bob then return true
 * 		2.2)If the person does not exist return false
 * 
 * Pseudo Code
 * List<Person> persons = Person.createRoster();
 * System.out.println(persons.stream()
 * 							 .filter(person -> person.name == "Bob" ? true: false));
 *
 *
 */

public class PersonChecker {
	
	public static void main(String[] args) {
		
		List<Person> persons = Person.createRoster();
		
		System.out.println(persons.stream()
								  .filter(person -> person.name == "Bob" ? true: false));
		
	}

}
