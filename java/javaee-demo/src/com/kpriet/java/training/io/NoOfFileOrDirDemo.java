package com.kpriet.java.training.io;

/*
Requirements : 
    Number of files in a directory and number of directories in a directory.
 
Entities :
    NoOfFilesAndDirDemo
  
Method Signature :
    public static void main(String[] args)
  
Jobs To Be Done:
    1) Create a reference for File with file as constructor argument.
    2) Print the number of files in the directory
    3) Add all the files and directories to the file array.
    4) initialize a integer variable with 0.
    5) for each item in the array
       5.1) check whether the item is directory
       5.2) if yes, add 1 to the integer variable
    6)Print the value of integer variable.
 
PseudoCode:
  
class NoOfFilesAndDir {
 
    public static void main(String[] args) {
        File file = new File("C:\\Users\\DELL\\eclipse-workspace\\javaee-demo");
        
        File[] files = file.listFiles();
        int Dir = 0;
        int Files = 0;
        
        for(File element : files) {
            Dir += element.isDirectory() ? 1 : 0;
            Files += element.isFile() ? 1 : 0;
        }
        
        System.out.println("Number of directories in this directory : " + Dir);
        System.out.println("Number of files in this directory : " + Files);
    }

}

 */
import java.io.File;

public class NoOfFileOrDirDemo {
    
    public static void main(String[] args) {
        File file = new File("C:\\Users\\DELL\\eclipse-workspace\\javaee-demo");
        
        File[] files = file.listFiles();
        int Dir = 0;
        int Files = 0;
        
        for(File element : files) {
            Dir += element.isDirectory() ? 1 : 0;
            Files += element.isFile() ? 1 : 0;
        }
        
        System.out.println("Number of directories in this directory : " + Dir);
        System.out.println("Number of files in this directory : " + Files);
    }

}

