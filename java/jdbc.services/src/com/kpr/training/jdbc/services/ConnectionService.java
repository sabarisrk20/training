package com.kpr.training.jdbc.services;
import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCodesvalues;
import java.sql.Connection;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

/* Create a Connection Class
 
 Entity
  * ConnectionService

 Method Signature
  * public static HikariDataSource getHikari()
  * public static void init() throws Exception
  * public static Connection getConnection() throws Exception
  * public static void releaseConnection() 
  * public static void commitRollback(boolean result) throws Exception

Jobs to be Done
 * Create a private HikariDataSource object called datasource.
 * Create a private ThreadLocal name thread.
 * Create a method getHikari that returns HikariDataSourcce
 * In getHikari() Method
    -> try
      => Create a String configFile and pass the db.properties file path 
      => Create a Object name HikariConfig name object name config and invoke theconfigFile
      => Set auto Commit false and set maximum pool size
      => invoke HikariDataSource name config to dataSource
      => return dataSource
    -> catch Exception
      => throw app Exception
 * Create a method init()
 * In init() method
      => set the thread by invoking the method getHikari().getConnection().
 * Create a method getConnection() returns connection
 * In getConnection() method 
      => if thread.get is equal to null
           -> call init() method
      => return thread.get()
 * Create a method releaseConnection() returns connection
 * In releaseConnection() method
      => close the thread
      => remove the thread
 * Create a method commitRollback(boolean result)
      => if result is true 
         -> commit
      => else
         -> rollback
         
Pseudo Code
public class ConnectionService{
    private static HikariDataSource dataSource;
	private static ThreadLocal<Connection> thread = new ThreadLocal<>();
    public static HikariDataSource getHikari(){
    	try {
              String configFile = "C:/Users/Sunil kumar/eclipse-workspace/jdbc.services/resorces/db.properties";
              HikariConfig config = new HikariConfig(configFile);   
              config.setAutoCommit(false);
              config.setMaximumPoolSize(3);  
              dataSource = new HikariDataSource(config);           
            } catch (Exception e){
    			throw new AppException(ErrorCodesvalues.ERROR01);
    		}
    	 return dataSource;
    }
    
    public static void init() throws Exception {
    	thread.set(getHikari().getConnection());
    }

	public static Connection getConnection() throws Exception {
		if(thread.get() == null) {
			init();
		}
		return thread.get();
	}

	public static void releaseConnection() {		
		try {
			thread.get().close();
			thread.remove();
			
		} catch(Exception e) {
			throw new AppException(ErrorCodesvalues.ERROR08);
		}
	}
	
	public static void commitRollback(boolean result) throws Exception{
		if(result == true) {
			thread.get().commit();
		} else {
			thread.get().rollback();
		}
	}
	
}
	
}
 */

public class ConnectionService{
    private static HikariDataSource dataSource;
	private static ThreadLocal<Connection> thread = new ThreadLocal<>();
    public static HikariDataSource getHikari(){
    	try {
              String configFile = "C:/Users/Sunil kumar/eclipse-workspace/jdbc.services/resorces/db.properties";
              HikariConfig config = new HikariConfig(configFile);   
              config.setAutoCommit(false);
              config.setMaximumPoolSize(3);  
              dataSource = new HikariDataSource(config);           
            } catch (Exception e){
    			throw new AppException(ErrorCodesvalues.ERROR01);
    		}
    	 return dataSource;
    }
    
    public static void init() throws Exception {
    	thread.set(getHikari().getConnection());
    }

	public static Connection getConnection() throws Exception {
		if(thread.get() == null) {
			init();
		}
		return thread.get();
	}

	public static void releaseConnection() {		
		try {
			thread.get().close();
			thread.remove();
			
		} catch(Exception e) {
			throw new AppException(ErrorCodesvalues.ERROR08);
		}
	}
	
	public static void commitRollback(boolean result) throws Exception{
		if(result == true) {
			thread.get().commit();
		} else {
			thread.get().rollback();
		}
	}
	
}
