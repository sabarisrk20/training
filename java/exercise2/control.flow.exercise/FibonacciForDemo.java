package in.kpriet.training.java.module;

public class FibonacciForDemo {
    public void fiboFor(int num3) {
        int num1=0,num2=1,temp;
        for(int iteration = 1; iteration <= num3; iteration++) {
            System.out.print(num1 + " ");
            temp = num1 + num2;
            num1 = num2;
            num2 = temp;
        }
    }
}