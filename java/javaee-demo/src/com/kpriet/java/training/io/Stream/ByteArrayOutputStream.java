package com.kpriet.java.training.io.Stream;

/*
Requirement:
    To write data to multiple files together using byteArray and outputStream.
 
Entity:
    ByteArrayOutputStream 

Function Declaration:

    public static void main(String[] args)
 
Jobs to be done:

    1. Create OutputStreams and pass the parameter as path of the file1. 
    2. Create OutputStreams and pass the parameter as path of the file2.
    3. Initialize the string value to be written in the file to string.
    4. Convert the string to byte array named bytes.
    5. Write the bytes to file1 using OutputStream.
    6. Write the bytes to file2 using OutputStream.
    
PseudoCode:

public class WriteDataDemo {
    
    public static void main(String[] args) throws IOException {
        
        OutputStream outputStream = new FileOutputStream("D:\\text.txt");
        OutputStream outputStream1 = new FileOutputStream("D:\\textfile.txt");
        String string = "Welcom" + "back" + "friends"; 

        byte[] bytes = string.getBytes(); 
        outputStream.write(bytes);
        outputStream1.write(bytes);
        System.out.println("Written successfully");
        outputStream.close();
        outputStream1.close();
    }
}
*/

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class ByteArrayOutputStream {

    public static void main(String[] args) throws IOException {
        
        OutputStream outputStream = new FileOutputStream("D:\\text.txt");
        OutputStream outputStream1 = new FileOutputStream("D:\\textfile.txt");
        String string =  "Welcom" + "back" + "friends"; 

        byte[] bytes = string.getBytes(); 
        outputStream.write(bytes);
        outputStream1.write(bytes);
        System.out.println("Written successfully");
        outputStream.close();
        outputStream1.close();
    }
}
